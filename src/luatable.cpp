#include "luatable.h"

using namespace std;

void LuaTable::add(const string &name, int value)
{
	values_.insert(make_pair(name, value));
}

size_t LuaTable::size() const
{
	return values_.size();
}
