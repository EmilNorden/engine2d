#ifndef WORLD_H_
#define WORLD_H_

#include <map>
#include <vector>
#include "entity.h"
#include "collidable_entity.h"
#include "quadtree.h"
#include "worldlayer.h"
#include "collisionhandler.h"
#include "lua_interop_object.h"

class GameTime;
class GLRenderer;
class Viewport;
union SDL_Event;
class EventRouter;

class World
{
private:
	bool sortEntities_;
	std::map<std::string, EntityPtr> entityMap_;
	std::vector<EntityPtr> entities_;
	QuadTree<EntityPtr> entityTree_;
	CollisionHandler collisionHandler_;
public:
	World();
	~World();

	
	
	void		add(EntityPtr &entity);
	void		buildQuadTree(int treshold, int maxDepth);
	void		drawCulled(size_t currentFrame, const GameTime &gameTime, const Viewport &viewport, const GLRenderer &renderer);
	EntityPtr	getEntity(const std::string &name);
	void		init(EventRouter &e);
	void		remove(const std::string &name);
	void		setSortFlag();
	void		update(size_t frame, const GameTime  &gameTime);
	void		mouseClickHandler(const LuaInteropObjectPtr &e);

	void updateVisibleEntities(size_t currentFrame, const Viewport &viewport);

	std::vector<EntityPtr>::const_iterator entitiesBegin() const { return entities_.cbegin(); }
	std::vector<EntityPtr>::const_iterator entitiesEnd() const { return entities_.cend(); }
};

#endif
