#ifndef LUA_TABLE_H_
#define LUA_TABLE_H_

#include <map>
#include <string>
#include <cstddef>

class LuaTable
{
private:
	std::map<std::string, int> values_;
public:
	void add(const std::string &name, int value);
	size_t size() const;
	std::map<std::string, int>::const_iterator cbegin() { return values_.cbegin(); }
	std::map<std::string, int>::const_iterator cend() { return values_.cend(); }
};

#endif
