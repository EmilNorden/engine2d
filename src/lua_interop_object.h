#ifndef LUA_INTEROP_OBJECT_H_
#define LUA_INTEROP_OBJECT_H_

#include "luatable.h"
#include <memory>

class LuaInteropObject
{
public:
	virtual LuaTable buildLuaTable() const = 0;
};

typedef std::unique_ptr<LuaInteropObject> LuaInteropObjectPtr;

#endif
