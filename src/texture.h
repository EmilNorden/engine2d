#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "sdl/SDL_opengl.h"
struct SDL_Surface;

class Texture
{
private:
	GLuint texture_;
	GLint nOfColors_;
	GLenum textureFormat_;
	SDL_Surface *surface_;
	int width_;
	int height_;
public:
	Texture(SDL_Surface *surface);
	~Texture();

	int width() const { return width_; }
	int height() const { return height_; }
	GLuint glTexture() const { return texture_; }
};

#endif