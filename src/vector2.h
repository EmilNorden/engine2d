#ifndef VECTOR2_H_
#define VECTOR2_H_

#include <cmath>

template <class T>
class Vector2
{
private:
	T v_[2];
public:
	Vector2() {
		v_[0] = T();
		v_[1] = T();
	}

	Vector2(T x, T y) {
		v_[0] = x;
		v_[1] = y;
	}

	inline
	T& x() {
		return v_[0];
	}

	inline
	T& y() {
		return v_[1];
	}

	inline
	T x() const {
		return v_[0];
	}

	inline
	T y() const {
		return v_[1];
	}

	Vector2 operator-(const Vector2 &other) const {
		return Vector2(v_[0] - other.v_[0], v_[1] - other.v_[1]);
	}

	Vector2 operator+(const Vector2 &other) const {
		return Vector2(v_[0] + other.v_[0], v_[1] + other.v_[1]);
	}

	Vector2 operator*(const Vector2 &other) const {
		return Vector2(v_[0] * other.v_[0], v_[1] * other.v_[1]);
	}

	bool operator!=(const Vector2 &other) const {
		return v_[0] != other.v_[0] || v_[1] != other.v_[1];
	}

	Vector2 operator*(const double f) const {
		return Vector2(v_[0] * f, v_[1] * f);
	}

	Vector2 operator/(const double d) const {
		return Vector2(v_[0] / d, v_[1] / d);
	}

	double dot(const Vector2 &other) const {
		return v_[0] * other.v_[0] + v_[1] * other.v_[1];
	}

	double length() const {
		return sqrt(pow(v_[0], 2) + pow(v_[1], 2));
	}

	void flip() {
		v_[0] = -v_[0];
		v_[1] = -v_[1];
	}

	void normalize() {
		double len = length();
		v_[0] /= len;
		v_[1] /= len;
	}

	const T *arr() const { return v_; }
};

typedef Vector2<int> Vector2i;
typedef Vector2<double> Vector2d;

#endif