#ifndef EVENT_PARAMS_H_
#define EVENT_PARAMS_H_

#include "lua_interop_object.h"
#include "luatable.h"

class MouseMoveEventParam : public LuaInteropObject
{
private:
	int x_;
	int y_;
public:
	MouseMoveEventParam(int x, int y)
		: x_(x), y_(y)
	{
	}
	int x() const { return x_; }
	int y() const { return y_; }
	LuaTable buildLuaTable() const;
};

class MouseClickEventParam : public LuaInteropObject
{
private:
	int x_;
	int y_;
	int buttonIndex_;
public:
	MouseClickEventParam(int x, int y, int index)
		: x_(x), y_(y), buttonIndex_(index)
	{
	}
	int x() const { return x_; }
	int y() const { return y_; }
	int buttonIndex() const { return buttonIndex_; }
	LuaTable buildLuaTable() const;
};

class ApplicationQuitEventParam : public LuaInteropObject
{
public:
	LuaTable buildLuaTable() const;
};


class EmptyEventParam : public LuaInteropObject
{
	LuaTable buildLuaTable() const
	{
		return LuaTable();
	}
};

#endif
