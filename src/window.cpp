#include "window.h"
#include "sdl/SDL.h"
#include "error.h"

Window::Window(const std::string &title, int x, int y, int w, int h, unsigned int flags)
	: width_(w), height_(h)
{
	SDL_PTR_CALL((window_ = SDL_CreateWindow(title.c_str(), x, y, w, h, flags)));
}

Window::~Window()
{
	SDL_DestroyWindow(window_);
}