#ifndef TRANSFORMATION_H_
#define TRANSFORMATION_H_

#include "matrix.h"
#include <cstddef>

class Transformation
{
private:
	Matrix world_;
	bool worldIsDirty_;
	Vector2d scale_;
	Vector2d rotation_;
	Vector2d translation_;
	Vector2d offset_;
	
	size_t updatedInFrame_;
public:
	
	Transformation();
	~Transformation();

	const Matrix &world() const;
	
	const Vector2d &scale() const;
	Transformation &setScale(const Vector2d &scale);

	const Vector2d &rotation() const;
	Transformation &setRotation(const Vector2d &rotation);

	const Vector2d &translation() const;
	Transformation &setTranslation(const Vector2d &translation);

	const Vector2d &offset() const;
	Transformation &setOffset(const Vector2d &offset);

	const Vector2d absoluteTranslation() const;

	bool applyChanges(size_t frame);

	size_t updatedInFrame() const;
};

#endif
