#include "world.h"
#include "viewport.h"
#include "gl_renderer.h"
#include "entity.h"
#include "eventrouter.h"
#include <algorithm>
#include <stdexcept>
#include <boost/format.hpp>

bool sortEntitiesByZIndex (EntityPtr i, EntityPtr j)
{ 
	return (i->currentLayer() == j->currentLayer() ? 
		i->zindex() < j->zindex() :
		i->currentLayer() < j->currentLayer());
}

World::World()
	: sortEntities_(false)
{
}

World::~World()
{
}

void World::init(EventRouter &e)
{
	e.add(SDL_MOUSEBUTTONDOWN, EventCallbackPtr(new HostCallback<const LuaInteropObjectPtr&>(std::bind(&World::mouseClickHandler, this, std::placeholders::_1))));
}

void World::setSortFlag()
{
	sortEntities_ = true;
}

void World::add(EntityPtr &entity)
{
	if(!entity->name().empty())
	{
		if(entityMap_.find(entity->name()) != entityMap_.end())
			throw std::runtime_error((boost::format("World::add: An entity with name '%1' already exists.") % entity->name()).str().c_str());

		entityMap_.insert(std::pair<std::string, EntityPtr>(entity->name(), entity));
	}

	entities_.push_back(entity);
	sortEntities_ = true;
}

void World::remove(const std::string &name)
{
	auto it = entityMap_.find(name);
	if(it != entityMap_.end())
	{
		entityMap_.erase(it);
	}
}

void World::buildQuadTree(int treshold, int maxDepth)
{
	entityTree_.build(treshold, maxDepth, entitiesBegin(), entitiesEnd());
}

void World::drawCulled(size_t currentFrame, const GameTime &gameTime, const Viewport &viewport, const GLRenderer &renderer)
{
	size_t drawn = 0;
	for(auto it = entities_.begin(); it != entities_.end(); ++it)
	{
		glLoadIdentity();
		//glTranslated(-viewport.origin().x(), -viewport.origin().y(), 0);
		if((*it)->renderedFrame() == currentFrame) {
			(*it)->draw(gameTime, renderer);
			drawn++;
		}
	}
}

EntityPtr World::getEntity(const std::string &name)
{
	auto entity = entityMap_.find(name);
	if(entity == entityMap_.end())
		return nullptr;

	return entity->second;
}

void World::update(size_t frame, const GameTime  &gameTime)
{
	collisionHandler_.collisionCheck(frame, entities_, entityTree_);
	//collisionHandler_.runPass(frame, entitiesBegin(), entitiesEnd());
	if(sortEntities_)
	{
		sortEntities_ = false;

		std::sort(entities_.begin(), entities_.end(), sortEntitiesByZIndex);
	}
}

void World::updateVisibleEntities(size_t currentFrame, const Viewport &viewport)
{
	std::function<void(const EntityPtr&)> func_obj = [&](const EntityPtr &p) 
	{
		p->setRenderedFrame(currentFrame);
	};

	entityTree_.processTree(viewport.bounds(),
		func_obj);

}

void World::mouseClickHandler(const LuaInteropObjectPtr &e)
{
	Vector2d coordinate(0,0);//(e.button.x, e.button.y);
	AABB bounds(Vector2d(/*e.button.x, e.button.y*/0,0), coordinate + Vector2d(1, 1));

	std::function<void(const EntityPtr&)> func_obj = [&](const EntityPtr &p) 
	{
		p->transformation().setRotation(p->transformation().rotation() + Vector2d(0.01, 0.01));
	};

	entityTree_.processTree(bounds, func_obj);
}
