#ifndef TEXTBOX_H_
#define TEXTBOX_H_

#include <memory>
#include <string>
#include "sdl/SDL_events.h"
#include "sdl/SDL_keyboard.h"
#include "vector2.h"
#include "aabb.h"
#include "lua_interop_object.h"

class Font;
class GLRenderer;
class TextSprite;
class GameTime;
class EventRouter;
class LuaInteropObject;

class TextBox
{
private:
	std::string name_;
	std::shared_ptr<Font> font_;
	std::string text_;

	int charHeight_, charWidth_;
	int caretPosition_;
	bool caretState_;

	bool active_;
	AABB bounds_;
	Vector2d position_;
	Vector2d size_;
	std::unique_ptr<TextSprite> textSprite_;

	int inputCbkIndex_, keydownCbkIndex_;
	void textInput(const LuaInteropObjectPtr &e);
public:
	TextBox();
	~TextBox();

	void setActive(bool active);

	void init(const std::shared_ptr<Font> &font, EventRouter &events);

	void render(const GLRenderer &renderer);
	void update(const GameTime &time);

	Vector2d position() const { return position_; }
	void setPosition(const Vector2d &pos);

	Vector2d size() const { return size_; }
	/* The size passed to setSize is merely a suggestion, the TextBox will calculate a size based on this. */
	void setSize(const Vector2d &size);
};

#endif
