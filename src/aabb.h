#ifndef AXISALIGNEDBOUNDINGBOX_H_
#define AXISALIGNEDBOUNDINGBOX_H_

#include "vector2.h"

class OBB;

class AABB
{
private:
	Vector2d min_;
	Vector2d max_;
public:
	AABB();
	~AABB();
	AABB(const Vector2d &min, const Vector2d &max);

	bool contains(const Vector2d &point) const;
	bool intersects(const AABB &other) const;
	bool intersects(const OBB &other) const;
	void inflate(const AABB &other);
	void inflate(const OBB &other);
	Vector2d minvec() const { return min_; }
	Vector2d maxvec() const { return max_; }

	void setMax(const Vector2d &max) { max_ = max; }
	void setMin(const Vector2d &min) { min_ = min; }
};

#endif