#include "textsprite.h"
#include "sdl/extensions/SDL_ttf.h"
#include "gl_renderer.h"

TextSprite::TextSprite(const std::shared_ptr<Font> &font, const Color &clr, const std::string &text)
	: color_(clr), font_(font)
{
	text_ = text;
	buildTexture();
}

std::string TextSprite::text() const
{
	return text_;
}

void TextSprite::setText(const std::string &value)
{
	if(text_ != value)
	{
		text_ = value;

		buildTexture();
	}
}

void TextSprite::buildTexture()
{
	SDL_Color c;
	c.a = color_.a * 255;
	c.r = color_.r * 255;
	c.g = color_.g * 255;
	c.b = color_.b * 255;
	SDL_Surface *surface;
	if(text_.empty())
		surface = TTF_RenderUTF8_Blended(font_->getSDLFont(), " ", c); // TTF_Render** does not seem to handle empty strings well, so render a whitespace
	else
		surface = TTF_RenderUTF8_Blended(font_->getSDLFont(), text_.c_str(), c);
	texture_ = std::make_shared<Texture>(surface);

	SDL_FreeSurface(surface);
}

void TextSprite::draw(const GameTime &gameTime, const GLRenderer &render)
{
	render.renderTexture(texture_, 0, 0, texture_->width(), texture_->height());
}

int TextSprite::width() const
{
	return texture_->width();
}

int TextSprite::height() const
{
	return texture_->height();
}