#ifndef STRING_DESCRIPTOR_H_
#define STRING_DESCRIPTOR_H_

#include "font.h"

#include <string>
#include <memory>

struct StringDescriptor
{
public:
	std::string text;
	size_t size;
	const std::shared_ptr<Font> font;

	StringDescriptor(const std::string &t, size_t s, const std::shared_ptr<Font> f);

	bool operator <(const StringDescriptor &rhs) const;
};

#endif