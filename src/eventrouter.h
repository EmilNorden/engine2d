#ifndef EVENT_ROUTER_H_
#define EVENT_ROUTER_H_

#include "sdl/SDL_events.h"
#include "hook_provider.h"
#include "callback.h"
#include <map>
#include <memory>
#include <vector>
#include <functional>

class EngineCore;
class LuaInteropObject;
typedef std::unique_ptr<LuaInteropObject> LuaInteropObjectPtr;

typedef std::unique_ptr<Callback<const LuaInteropObjectPtr&>> EventCallbackPtr;

class EventRouter : public HookProvider
{
private:
	std::map<SDL_EventType, std::vector<EventCallbackPtr>> callbacks_;
public:
	EventRouter();

	void add(const SDL_EventType &type, EventCallbackPtr &callback);
	void add(const SDL_EventType &type, EventCallbackPtr &&callback);
	void remove(const SDL_EventType &type, EventCallbackPtr &callback);
	void remove(const SDL_EventType &type, EventCallbackPtr &&callback);
	LuaInteropObjectPtr getEventInteropObject(const SDL_Event &e);

	void update();

	std::vector<LuaHook> getHooks() override;
};

#endif
