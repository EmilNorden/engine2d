#ifndef ANIMATED_SPRITE_H_
#define ANIMATED_SPRITE_H_

#include "sprite.h"
#include <memory>

class Texture;

class AnimationFrame
{
private:
	std::shared_ptr<Texture> texture_;
	int duration_;
	Vector2d offset_;
public:
	AnimationFrame(const std::shared_ptr<Texture> &texture, int duration, const Vector2d &offset);
	~AnimationFrame();

	const std::shared_ptr<Texture> &texture() const { return texture_; }
	int duration() const { return duration_; }
	Vector2d offset() const { return offset_; }
};

class AnimatedSprite : public Sprite
{
private:
	std::vector<AnimationFrame> frames_;
	size_t currentFrameIndex_;
	int frameTimer_;

	int maxWidth_;
	int maxHeight_;

	void calculateHeight();
public:
	AnimatedSprite(std::vector<AnimationFrame> &frames);
	AnimatedSprite();

	void draw(const GameTime &gameTime, const GLRenderer &renderer);

	int width() const { return maxWidth_; }
	int height() const { return maxHeight_; }
};

#endif