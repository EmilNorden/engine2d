#include "viewport.h"

Viewport::Viewport(const Vector2d &origin, const Vector2d &size)
	: origin_(origin), size_(size), bounds_(origin, origin + size)
{
}

Viewport::~Viewport()
{
}