#include "contentloader.h"
#include "sdl/extensions/SDL_image.h"
#include "sdl/extensions/SDL_ttf.h"
#include "sdl/SDL_assert.h"
#include "font.h"
#include "texture.h"
#include "luastate.h"

#include <string>
#include <sstream>
#include <iostream>


ContentLoader::ContentLoader()
{
	
}

void ContentLoader::init(const std::string &folder)
{
	contentFolder_ = folder;
	// Append '/' to end of path if it does not exist
	char last = contentFolder_.at(contentFolder_.size() - 1);
	if(last != '/' && last != '\\')
		contentFolder_.push_back('/');

	errorTexture_ = loadTexture("textures/error.png");
	SDL_assert(errorTexture_);
}

std::shared_ptr<Texture> ContentLoader::loadTexture(const std::string &name)
{
	std::shared_ptr<Texture> tex = texCache_.get(name);

	if(!tex)
	{
		const std::string texPath = contentFolder_ + name;
		SDL_Surface *surface = IMG_Load(texPath.c_str());
		
		if(!surface)
		{
			std::cerr << "Failed to load texture " << texPath << std::endl;
			tex = errorTexture_;
		}
		else
		{
			tex = std::make_shared<Texture>(surface);
			//tex.reset(new Texture(surface));
			SDL_FreeSurface(surface);
			texCache_.insert(name, tex);
		}
	}

	return tex;
}

std::shared_ptr<Font> ContentLoader::loadFont(const std::string &name, size_t size)
{
	std::shared_ptr<Font> font = fontCache_.get(name);

	if(!font)
	{
		const std::string fontPath = contentFolder_ + name;
		TTF_Font *ttf = TTF_OpenFont(fontPath.c_str(), size);
		font = std::make_shared<Font>(ttf, name);
		//font.reset(new Font(ttf, name));
		fontCache_.insert(name, font);
	}

	return font;
}

std::shared_ptr<Texture> ContentLoader::loadString(const StringDescriptor &desc)
{
	std::shared_ptr<Texture> tex = stringCache_.get(desc);

	if(!tex)
	{
		SDL_Color c;
		c.r = c.g = c.b = 0;
		c.a = 255;
		SDL_Surface *surface = TTF_RenderText_Blended(desc.font->getSDLFont(), desc.text.c_str(), c); 
		tex = std::make_shared<Texture>(surface);
		//tex.reset(new Texture(surface));
		SDL_FreeSurface(surface);
		stringCache_.insert(desc, tex);
	}

	return tex;
}

std::shared_ptr<LuaState> ContentLoader::loadScript(const std::string &name)
{
	// No caching for now.
	std::shared_ptr<LuaState> state = LuaState::makeShared();

	const std::string scriptPath = contentFolder_ + name;

	try
	{
		state->open(scriptPath, nullptr);
	}
	catch(LuaException &e)
	{
		std::cerr << "Loading script (" << scriptPath << ") threw exception: " << e.what() << std::endl;
	}

	return state;
}