#ifndef WINDOW_H_
#define WINDOW_H_

#include <string>


struct SDL_Window;

class Window
{
private:
	SDL_Window *window_;
	int width_;
	int height_;
public:
	Window(const std::string &title, int x, int y, int w, int h, unsigned int flags);
	~Window();

	SDL_Window *operator()() const { return window_; }
	SDL_Window *getSDLWindow() const { return window_; }

	int width() const { return width_; }
	int height() const { return height_; }
};

#endif