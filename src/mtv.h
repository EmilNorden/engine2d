#ifndef MINIMUM_TRANSLATION_VECTOR
#define MINIMUM_TRANSLATION_VECTOR

#include "vector2.h"

class MTV
{
private:
	Vector2d axis_;
	double overlap_;
public:
	MTV()
	{
	}

	Vector2d axis() const { return axis_; }
	double overlap() const { return overlap_; }

	void setAxis(const Vector2d &axis) { axis_ = axis; }
	void setOverlap(double overlap) { overlap_ = overlap; }
};

#endif