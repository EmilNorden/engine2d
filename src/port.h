#ifndef MESSAGEPORT_H_
#define MESSAGEPORT_H_

#include <vector>
#include <functional>

namespace messaging
{
	template <typename T>
	class Port
	{

	};

	template <typename T>
	class PortIn : public Port<T>
	{
	private:
		std::function<void(T)> callback_;
	public:
		PortIn(std::function<void(T)> callback);
		void receive(const T& data);
	};

	template <typename T>
	class PortOut : public Port<T>
	{
	private:
		std::vector<PortIn<T>> connections_;
	public:
		void propagate(const T& data)
		{
			for(auto it = connections_.cbegin(); it != connections_.cend(); ++it)
				(*it).receive(data);
		}
	};

	template <typename T>
	PortIn<T>::PortIn(std::function<void(T)> callback)
		: callback_(callback)
	{
	}
}

#endif