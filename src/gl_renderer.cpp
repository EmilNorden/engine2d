#include "gl_renderer.h"
#include "window.h"
#include "texture.h"
#include "font.h"
#include "color.h"
#include <lua.hpp>
#include <array>

GLRenderer *activeScriptRenderer;

GLRenderer::GLRenderer()
	: context_(nullptr)
{
}

GLRenderer::~GLRenderer()
{
	if(context_ != nullptr)
		SDL_GL_DeleteContext(context_);
}

void GLRenderer::init(const Window &window)
{
	context_ = SDL_GL_CreateContext(window.getSDLWindow());

	// TODO: Kasta lite exceptions till h�ger och v�nster!
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glViewport(0, 0, window.width(), window.height());
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, window.width(), window.height(), 0.0f, -1.0, 1.0f);
	
	/*glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);*/

	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
}

void GLRenderer::clear() const
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void GLRenderer::present(const Window &window) const
{
	SDL_GL_SwapWindow(window.getSDLWindow());
}

Vector2i GLRenderer::getViewportDimensions() const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector2i(viewport[2], viewport[3]);	
}

void GLRenderer::setClearColor(const Color &color)
{
	clearColor_ = color;
	glClearColor(color.r, color.g, color.b, color.a);
}

void GLRenderer::renderTexture(const std::unique_ptr<Texture> &tex, int x, int y, int w, int h) const
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex->glTexture());
	glBegin(GL_QUADS);
		//Bottom-left vertex (corner)
		glTexCoord2i(0, 0);
		glVertex3f(0, 0, 0.0f);
 
		//Bottom-right vertex (corner)
		glTexCoord2i(1, 0);
		glVertex3f(static_cast<GLfloat>(w), 0, 0.f);
 
		//Top-right vertex (corner)
		glTexCoord2i(1, 1);
		glVertex3f(static_cast<GLfloat>(w), static_cast<GLfloat>(h), 0.f);
 
		//Top-left vertex (corner)
		glTexCoord2i(0, 1);
		glVertex3f(0, static_cast<GLfloat>(h), 0.f);
	glEnd();
}

void GLRenderer::renderTexture(const std::shared_ptr<Texture> &tex, int x, int y, int w, int h) const
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex->glTexture());
	glBegin(GL_QUADS);
		//Bottom-left vertex (corner)
		glTexCoord2i(0, 0);
		glVertex3f(0, 0, 0.0f);
 
		//Bottom-right vertex (corner)
		glTexCoord2i(1, 0);
		glVertex3f(static_cast<GLfloat>(w), 0, 0.f);
 
		//Top-right vertex (corner)
		glTexCoord2i(1, 1);
		glVertex3f(static_cast<GLfloat>(w), static_cast<GLfloat>(h), 0.f);
 
		//Top-left vertex (corner)
		glTexCoord2i(0, 1);
		glVertex3f(0, static_cast<GLfloat>(h), 0.f);
	glEnd();
}

void GLRenderer::renderColor(float r, float g, float b, float a, int x, int y, int w, int h) const
{
	glDisable(GL_TEXTURE_2D);
	glColor4f(r, g, b, a);
	glBegin(GL_QUADS);
		//Bottom-left vertex (corner)
		glTexCoord2i(0, 0);
		glVertex3f(0, 0, 0.0f);
 
		//Bottom-right vertex (corner)
		glTexCoord2i(1, 0);
		glVertex3f(static_cast<GLfloat>(w), 0, 0.f);
 
		//Top-right vertex (corner)
		glTexCoord2i(1, 1);
		glVertex3f(static_cast<GLfloat>(w), static_cast<GLfloat>(h), 0.f);
 
		//Top-left vertex (corner)
		glTexCoord2i(0, 1);
		glVertex3f(0, static_cast<GLfloat>(h), 0.f);
	glEnd();
}

void GLRenderer::renderText(const std::shared_ptr<GUIText> &text) const
{

}

int renderGetClearColor(lua_State *L)
{
	return 0;
}

int renderSetClearColor(lua_State *L)
{
	int argc = lua_gettop(L);
	

		float a = static_cast<float>(lua_tonumber(L, 1));
		float r = static_cast<float>(lua_tonumber(L, 2));
		float g = static_cast<float>(lua_tonumber(L, 3));
		float b = static_cast<float>(lua_tonumber(L, 4));

		lua_getglobal(L, "systemInstances");

		void* result;
		lua_pushstring(L, "renderSetClearColor");
		lua_gettable(L, -2);  /* get background[key] */

		result = (void*)lua_topointer(L, -1);;
		lua_pop(L, 1);  /* remove number */
		
		GLRenderer *rend = reinterpret_cast<GLRenderer*>(result);

		rend->setClearColor(Color(a, r, g, b));

	return 0;
}

int renderGetViewportDimensions(lua_State *L)
{
	lua_gettop(L);

	lua_getglobal(L, "systemInstances");

	void *result;	
	lua_pushstring(L, "renderGetViewportDimensions");
	lua_gettable(L, -2);
	
	result = (void*)lua_topointer(L, -1);
	lua_pop(L, 1);

	GLRenderer *rend = reinterpret_cast<GLRenderer*>(result);

	Vector2i dim = rend->getViewportDimensions();

	lua_pushnumber(L, dim.x());
	lua_pushnumber(L, dim.y());

	return 2;
}

std::vector<LuaHook> GLRenderer::getHooks()
{
	std::array<LuaHook, 3> funcs = { 
		LuaHook("renderGetClearColor", renderGetClearColor, this),
		LuaHook("renderSetClearColor", renderSetClearColor, this),
		LuaHook("renderGetViewportDimensions", renderGetViewportDimensions, this)
	};

	return std::vector<LuaHook>(funcs.begin(), funcs.end());
}
