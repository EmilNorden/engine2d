#include "collisionhandler.h"
#include "sprite.h"
#include <functional>

void CollisionHandler::checkCollision(EntityPtr a, EntityPtr b)
{
	MTV mtv;
	if(a->bounds().intersects(b->bounds(), mtv))
	{
		
			
		
	}
}

void CollisionHandler::collisionCheck(size_t frame, std::vector<EntityPtr> &entities, QuadTree<EntityPtr> &entityTree)
{
	for(auto it = entities.begin(); it != entities.end(); ++it)
	{
		if((*it)->isSolid() && (*it)->transformation().updatedInFrame() == frame)
		{
			std::function<void(const EntityPtr&, MTV&)> func_obj = [&](const EntityPtr &p, MTV &mtv) 
			{
				if((*it)->identifier() != p->identifier())
				{
					if(p->isSolid())
					{
						Vector2d offset = mtv.axis() * (mtv.overlap() + 0.1);

						// Should this not take offset into account?
						//Vector2d obstacleDirection = (p->transformation().translation() + p->transformation().offset())  - ((*it)->transformation().translation() + (*it)->transformation().offset());
						Vector2d obstacleDirection = (p->transformation().translation())  - ((*it)->transformation().translation());
						obstacleDirection.normalize();
						if(mtv.axis().dot(obstacleDirection) > 0)
							offset.flip();

						

						(*it)->transformation().setTranslation((*it)->transformation().translation() + (offset * (1.0 - p->pushable())));
						(*it)->updateTransformation(frame);

						p->transformation().setTranslation(p->transformation().translation() + (offset * -1 * (p->pushable())));
						p->updateTransformation(frame);
						//p->transformation().setScale(Vector2d(p->transformation().scale().x() + 0.01, p->transformation().scale().y()));

					}
				}
				
			};
			
			entityTree.processTree((*it)->bounds(),
				func_obj);
		}
	}
}
