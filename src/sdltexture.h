#ifndef SDLTEXTURE_H_
#define SDLTEXTURE_H_

#include <cstddef>

struct SDL_Texture;

class SDLTexture
{
private:
	SDL_Texture *texture_;
	int width_;
	int height_;
public:
	SDLTexture(SDL_Texture *texture);
	~SDLTexture();

	size_t width() const { return width_; }
	size_t height() const { return height_; }

	SDL_Texture *getSDLTexture() const { return texture_; }
};

#endif
