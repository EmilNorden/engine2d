#include "transformation.h"

Transformation::Transformation()
{
	translation_.x() = translation_.y() = rotation_.x() = rotation_.y() = 0;
	scale_.x() = scale_.y() = 1;
}

Transformation::~Transformation()
{
}

const Matrix &Transformation::world() const
{
	return world_;
}
	
const Vector2d &Transformation::scale() const
{
	return scale_;
}

Transformation &Transformation::setScale(const Vector2d &scale)
{
	if(scale != scale_)
	{
		scale_ = scale;
		worldIsDirty_ = true;
	}

	return *this;
}

const Vector2d &Transformation::rotation() const
{
	return rotation_;
}

Transformation &Transformation::setRotation(const Vector2d &rotation)
{
	if(rotation != rotation_)
	{
		rotation_ = rotation;
		worldIsDirty_ = true;
	}

	return *this;
}

const Vector2d &Transformation::translation() const
{
	return translation_;
}

Transformation &Transformation::setTranslation(const Vector2d &translation)
{
	if(translation != translation_)
	{
		translation_ = translation;
		worldIsDirty_ = true;
	}

	return *this;
}

bool Transformation::applyChanges(size_t frame)
{
	if(worldIsDirty_)
	{
		updatedInFrame_ = frame;
		worldIsDirty_ = false;

		Matrix transMatrix = Matrix::createTranslation(translation_);
		Matrix rotatMatrix = Matrix::createRotation(rotation_);
		Matrix scaleMatrix = Matrix::createScale(scale_);

		
		Matrix offsetMatrix = Matrix::createTranslation(offset_);
		world_ = offsetMatrix * scaleMatrix  * rotatMatrix * transMatrix;

		return true;
	}

	return false;
}

const Vector2d &Transformation::offset() const
{
	return offset_;
}
Transformation &Transformation::setOffset(const Vector2d &offset)
{
	if(offset != offset_)
	{
		worldIsDirty_ = true;
		offset_ = offset;
	}

	return *this;
}

const Vector2d Transformation::absoluteTranslation() const
{
	return offset_ + translation_;
}

size_t Transformation::updatedInFrame() const
{
	return updatedInFrame_;
}