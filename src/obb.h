#ifndef ORIENTEDBOUNDINGBOX_H_
#define ORIENTEDBOUNDINGBOX_H_

#include "vector2.h"
#include "mtv.h"

class AABB;
class Transformation;

class OBB
{
private:
	Vector2d corners_[4];
	Vector2d transformedCorners_[4];
	
	Vector2d normals_[4];

	Vector2d normalLines_[4][2];

	void findMinMaxIn1DProjection(const Vector2d &axis, double &min, double &max) const;
	bool rangeIntersects(const Vector2d &firstRange, const Vector2d &secondRange) const;

	bool intersectsInternal(OBB &obb);
	double getOverlap(const Vector2d &range1, const Vector2d &range2);
	void recalcNormals();
	void recalcAxisAlignedNormals();
public:
	
	OBB(const Vector2d &v1, const Vector2d &v2, const Vector2d &v3, const Vector2d &v4);
	OBB(const AABB &aabb);

	bool intersects(OBB &other);
	bool intersects(const AABB &other);
	bool intersects(OBB &other, MTV &mtv);

	void render();

	void move(const Vector2d &delta) { for(int i = 0; i < 4; ++i) { corners_[i] = corners_[i] + delta; } }

	void updateTransformation(const Transformation &transf);

	friend class AABB;
};

#endif