#ifndef SPRITE_H_
#define SPRITE_H_

#include <memory>
#include <vector>

#include "transformation.h"

class GameTime;
class GLRenderer;
class Texture;

class Sprite
{
protected:
	Transformation transformation_;
public:
	virtual void draw(const GameTime &gameTime, const GLRenderer &renderer) = 0;
	virtual int width() const = 0;
	virtual int height() const = 0;

	Transformation &transformation() { return transformation_; }
};

class SimpleSprite : public Sprite
{
private:
	std::shared_ptr<Texture> texture_;

public:

	SimpleSprite(const std::shared_ptr<Texture> &texture);
	void draw(const GameTime &gameTime, const GLRenderer &renderer);
	int width() const;
	int height() const;
};

#endif