#ifndef ERROR_H_
#define ERROR_H_

void sdlError(const char *file, int line);
void ttfError(const char *file, int line);

#define SDL_CALL(x) if(x == -1) sdlError(__FILE__, __LINE__);
#define SDL_PTR_CALL(x) if(!(x)) sdlError(__FILE__, __LINE__);

#define TTF_CALL(x) if(x == -1) ttfError(__FILE__, __LINE__);
#define TTF_PTR_CALL(x) if(!(x)) ttfError(__FILE__, __LINE__);

#endif