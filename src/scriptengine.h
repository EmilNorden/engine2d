#ifndef SCRIPT_ENGINE_H_
#define SCRIPT_ENGINE_H_

#include <functional>
#include <map>
#include <string>
#include <utility>
#include <memory>

class LuaState;
class GLRenderer;
class HookProvider;
class World;
struct lua_State;
class ContentLoader;

class ScriptEngine
{
private:
	std::map<std::string, std::pair<std::function<int(lua_State*)>, void*>> hooks_;
	/*std::map<const lua_State*, std::weak_ptr<LuaState>> stateLookup_;*/
public:
	void init(GLRenderer *renderer, World *world, ContentLoader *content);
	void registerHooks(HookProvider &provider);
	void registerHook(const std::string &name, std::function<int(lua_State*)> &func, void *p);
	void hookScript(LuaState &state);
	void tick(LuaState &state);

	/* When lua calls hooks, it passes a lua_State* to the function. Certain hooks, like addEventRouting, will store  */
	void registerState(const std::shared_ptr<LuaState> &state);
	std::shared_ptr<LuaState> getState(const lua_State* state);

	static ScriptEngine *getActiveInstance();
};


#endif