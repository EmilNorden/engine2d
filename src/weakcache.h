#ifndef WEAKCACHE_H_
#define WEAKCACHE_H_

#include <memory>
#include <map>
#include <string>

#include "error.h"
#include "sdltexture.h"
#include "sdl/SDL_thread.h"

template <typename TKey, typename T>
class WeakCache
{
private:
	SDL_mutex *mutex_;
	std::map<TKey, std::weak_ptr<T>> cache_;

public:
	WeakCache();
	~WeakCache();
	bool getInternal(const TKey &key, std::shared_ptr<T> &result);
	std::shared_ptr<T> get(const TKey &key);
	void insert(const TKey &key, std::shared_ptr<T> value);
};

template <typename TKey, typename T>
WeakCache<TKey, T>::WeakCache()
{
	SDL_PTR_CALL((mutex_ = SDL_CreateMutex()));

}

template <typename TKey, typename T>
WeakCache<TKey, T>::~WeakCache()
{
	SDL_DestroyMutex(mutex_);
}

template <typename TKey, typename T>
bool WeakCache<TKey, T>::getInternal(const TKey &key, std::shared_ptr<T> &result)
{
	bool success = false;
	auto it = cache_.find(key);
	if(it != cache_.end())
	{
		auto shared = (*it).second.lock();
		result = shared;
		if(shared)
		{
			success = true;
		}
		else
		{
			cache_.erase(it);
		}
	}

	return success;
}

template <typename TKey, typename T>
std::shared_ptr<T> WeakCache<TKey, T>::get(const TKey &key)
{
	std::shared_ptr<T> result;
	SDL_LockMutex(mutex_);
	getInternal(key, result);
	SDL_UnlockMutex(mutex_);

	return result;
}

template <typename TKey, typename T>
void WeakCache<TKey, T>::insert(const TKey &key, std::shared_ptr<T> value)
{
	SDL_LockMutex(mutex_);
	cache_.insert(std::pair<TKey, std::weak_ptr<T>>(key, value));
	SDL_UnlockMutex(mutex_);
}

#endif