#ifndef VIEWPORT_H_
#define VIEWPORT_H_

#include "vector2.h"
#include "aabb.h"

class Viewport
{
private:
	Vector2d size_;
	Vector2d origin_;
	AABB bounds_;
public:
	Viewport(const Vector2d &origin, const Vector2d &size);
	~Viewport();

	
	Vector2d size() const { return size_; }
	Vector2d origin() const { return origin_; }
	void setOrigin(const Vector2d &origin) { origin_ = origin; bounds_ = AABB(origin_, origin_ + size_); }
	const AABB &bounds() const { return bounds_; }
};

#endif