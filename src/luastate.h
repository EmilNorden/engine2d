#ifndef LUA_STATE_H_
#define LUA_STATE_H_

#include <string>
#include <exception>
#include <map>
#include <memory>

class LuaTable;
struct lua_State;

enum class LuaError
{
	None = 0,
	YIELD = 1,
	ERRRUN = 2,
	ERRSYNTAX = 3,
	ERRMEM = 4,
	ERRERR = 5,
	ERRFILE = 6
};

class LuaException : public std::exception
{
private:
	LuaError error_;
	std::string luaMessage_;
	std::string message_;
public:
	LuaException(const LuaError &error, const std::string &luaMessage);

	const char *what() const noexcept (true);
};

class LuaState
{
private:
	static std::map<lua_State*, std::weak_ptr<LuaState>> stateLookup_;
	lua_State *state_;

	inline void inspectErrors(int error);
	LuaState();
	LuaState(lua_State *L);

	static std::shared_ptr<LuaState> insertShared(lua_State *L);
public:
	
	~LuaState();

	void open(const std::string &path, void*p);

	void call(const std::string &func);

	void pcall(const std::string &func);

	void dostring(const std::string &script);

	void pushTable(LuaTable &table);

	/*void load(const std::string &script);*/

	lua_State *state() const { return state_; }

	bool operator==(const LuaState &other);

	static std::string getErrorText(const LuaError &err);

	static std::shared_ptr<LuaState> makeShared(lua_State *L);
	static std::shared_ptr<LuaState> makeShared();
};

#endif
