#ifndef QUADTREE_H_
#define QUADTREE_H_

#include "aabb.h"
#include "obb.h"
#include "mtv.h"
#include <vector>
#include <memory>
#include <vector>
#include <functional>

#include "entity.h"

template <typename T>
class QuadTree
{
private:
	template <typename TQuad>
	struct Quad
	{
	public:
		AABB bounds;
		std::vector<TQuad> objects;
		std::shared_ptr<Quad<TQuad>> quads;
	
		Quad() { }
	};


	Quad<T> root_;

	void buildQuad(size_t treshold, size_t maxDepth, size_t depth, Quad<T> &quad);
	void processTreeInternal(const Quad<T> &quad, const AABB &bounds, std::function<void(const T&)> &callback);
	void processTreeInternal(const Quad<T> &quad, OBB &bounds, std::function<void(const T&, MTV&)> &callback);
	void processTreeInternal(const Quad<T> &quad, const Vector2d &point, std::function<void(const T&)> &callback);
public:
	QuadTree() { }

	template <typename Iter>
	void build(size_t treshold, size_t maxDepth, Iter a, Iter b, typename Iter::iterator_category *p = 0);
	void processTree(const AABB &bounds, std::function<void(const T&)> &callback);
	void processTree(OBB &bounds, std::function<void(const T&, MTV&)> &callback);
	void processTree(const Vector2d &point, std::function<void(const T&)> &callback);
};

template <typename T>
template <typename Iter>
void QuadTree<T>::build(size_t treshold, size_t maxDepth, Iter a, Iter b, typename Iter::iterator_category *p)
{
	root_.objects.clear();
	for(auto it = a; it != b; ++it)
	{
		root_.bounds.inflate((*it)->bounds());
		root_.objects.push_back(*it);
		//root_.objects.push_back((*it).get());
	}

	buildQuad(treshold, maxDepth, 0, root_);
}

template <typename T>
void QuadTree<T>::buildQuad(size_t treshold, size_t maxDepth, size_t depth, Quad<T> &quad)
{
	if(quad.objects.size() > treshold || depth >= maxDepth)
		return;
	Vector2d splitSize = (quad.bounds.maxvec() - quad.bounds.minvec()) / 2.0;
	quad.quads.reset(new Quad<T>[4], [](Quad<T> *p) { delete[] p; });
	
	for(int x = 0; x < 2; ++x)
	{
		for(int y = 0; y < 2; ++y)
		{
			Quad<T> &child = quad.quads.get()[(x * 2) + y];
			child.bounds.setMin(quad.bounds.minvec() + (splitSize * Vector2d(x, y)));
			child.bounds.setMax(child.bounds.minvec() + splitSize);
			
			for(int obj_i = quad.objects.size() - 1; obj_i >= 0; --obj_i)
			{
				if(child.bounds.intersects(quad.objects[obj_i]->bounds()))
				{
					child.objects.push_back(quad.objects[obj_i]);
				}
			}
			buildQuad(treshold, maxDepth, depth + 1, child);
		}
	}
}

template <typename T>
void QuadTree<T>::processTreeInternal(const Quad<T> &quad, const AABB &bounds, std::function<void(const T&)> &callback)
{
	if(quad.bounds.intersects(bounds))
	{
		if(quad.quads)
		{
			for(int i = 0; i < 4; ++i)
				processTreeInternal(quad.quads.get()[i], bounds, callback);
		}
		else
		{
			for(size_t i = 0; i < quad.objects.size(); ++i)
			{
				if(quad.objects[i]->bounds().intersects(bounds))
				{
					callback(quad.objects[i]);
				}
			}
		}
	}
}

template <typename T>
void QuadTree<T>::processTreeInternal(const Quad<T> &quad, OBB &bounds, std::function<void(const T&, MTV&)> &callback)
{
	if(quad.bounds.intersects(bounds))
	{
		if(quad.quads)
		{
			for(int i = 0; i < 4; ++i)
				processTreeInternal(quad.quads.get()[i], bounds, callback);
		}
		else
		{
			for(size_t i = 0; i < quad.objects.size(); ++i)
			{
				MTV mtv;
				if(quad.objects[i]->bounds().intersects(bounds, mtv))
				{
					callback(quad.objects[i], mtv);
				}
			}
		}
	}
}

template <typename T>
void QuadTree<T>::processTreeInternal(const Quad<T> &quad, const Vector2d &point, std::function<void(const T&)> &callback)
{
	if(quad.bounds.contains(point))
	{
		if(quad.quads)
		{
			for(int i = 0; i < 4; ++i)
				processTreeInternal(quad.quads.get()[i], point, callback);
		}
		else
		{
			for(size_t i = 0; i < quad.objects.size(); ++i)
			{
				if(quad.objects[i]->bounds().contains(point))
				{
					callback(quad.objects[i]);
				}
			}
		}
	}
}

template <typename T>
void QuadTree<T>::processTree(const AABB &bounds, std::function<void(const T&)> &callback)
{
	processTreeInternal(root_, bounds, callback);
}

template <typename T>
void QuadTree<T>::processTree(OBB &bounds, std::function<void(const T&, MTV&)> &callback)
{
	processTreeInternal(root_, bounds, callback);
}

template <typename T>
void QuadTree<T>::processTree(const Vector2d &point, std::function<void(const T&)> &callback)
{
	processTreeInternal(root_, point, callback);
}

#endif
