#include "matrix.h"
#include <cmath>

Matrix::Matrix()
{
	for(int x = 0; x < 4; ++x)
		for(int y = 0; y < 4; ++y)
			m_[x][y] = 0;
}

Matrix Matrix::createTranslation(const Vector2d &translation)
{
	Matrix result;
	result.m_[0][0] = 1;
	result.m_[1][1] = 1;
	result.m_[2][2] = 1;
	result.m_[3][0] = translation.x();
	result.m_[3][1] = translation.y();
	result.m_[3][2] = 0;
	result.m_[3][3] = 1;

	return result;
}

Matrix Matrix::createScale(const Vector2d &scale)
{
	Matrix result;
	result.m_[0][0] = scale.x();
	result.m_[1][1] = scale.y();
	result.m_[2][2] = 1;
	result.m_[3][3] = 1;

	return result;
}

Matrix Matrix::createRotation(const Vector2d &rotation)
{
	Matrix result;
	double num1 = cos(rotation.x());
    double num2 = sin(rotation.x());
    
    result.m_[0][0] = num1;
    result.m_[0][1] = num2;
    result.m_[0][2] = 0.0;
    result.m_[0][3] = 0.0;
    result.m_[1][0] = -num2;
    result.m_[1][1] = num1;
    result.m_[1][2] = 0.0;
    result.m_[1][3] = 0.0;
    result.m_[2][0] = 0.0;
    result.m_[2][1] = 0.0;
    result.m_[2][2] = 1;
    result.m_[2][3] = 0.0;
    result.m_[3][0] = 0.0;
    result.m_[3][1] = 0.0;
    result.m_[3][2] = 0.0;
    result.m_[3][3] = 1;
    return result;
}

Matrix Matrix::operator*(const Matrix &other)
{
	Matrix result;
	result.m_[0][0] =  ( m_[0][0] *  other.m_[0][0] +  m_[0][1] *  other.m_[1][0] +  m_[0][2] *  other.m_[2][0] +  m_[0][3] *  other.m_[3][0]);
    result.m_[0][1] =  ( m_[0][0] *  other.m_[0][1] +  m_[0][1] *  other.m_[1][1] +  m_[0][2] *  other.m_[2][1] +  m_[0][3] *  other.m_[3][1]);
    result.m_[0][2] =  ( m_[0][0] *  other.m_[0][2] +  m_[0][1] *  other.m_[1][2] +  m_[0][2] *  other.m_[2][2] +  m_[0][3] *  other.m_[3][2]);
    result.m_[0][3] =  ( m_[0][0] *  other.m_[0][3] +  m_[0][1] *  other.m_[1][3] +  m_[0][2] *  other.m_[2][3] +  m_[0][3] *  other.m_[3][3]);
    result.m_[1][0] =  ( m_[1][0] *  other.m_[0][0] +  m_[1][1] *  other.m_[1][0] +  m_[1][2] *  other.m_[2][0] +  m_[1][3] *  other.m_[3][0]);
    result.m_[1][1] =  ( m_[1][0] *  other.m_[0][1] +  m_[1][1] *  other.m_[1][1] +  m_[1][2] *  other.m_[2][1] +  m_[1][3] *  other.m_[3][1]);
    result.m_[1][2] =  ( m_[1][0] *  other.m_[0][2] +  m_[1][1] *  other.m_[1][2] +  m_[1][2] *  other.m_[2][2] +  m_[1][3] *  other.m_[3][2]);
    result.m_[1][3] =  ( m_[1][0] *  other.m_[0][3] +  m_[1][1] *  other.m_[1][3] +  m_[1][2] *  other.m_[2][3] +  m_[1][3] *  other.m_[3][3]);
    result.m_[2][0] =  ( m_[2][0] *  other.m_[0][0] +  m_[2][1] *  other.m_[1][0] +  m_[2][2] *  other.m_[2][0] +  m_[2][3] *  other.m_[3][0]);
    result.m_[2][1] =  ( m_[2][0] *  other.m_[0][1] +  m_[2][1] *  other.m_[1][1] +  m_[2][2] *  other.m_[2][1] +  m_[2][3] *  other.m_[3][1]);
    result.m_[2][2] =  ( m_[2][0] *  other.m_[0][2] +  m_[2][1] *  other.m_[1][2] +  m_[2][2] *  other.m_[2][2] +  m_[2][3] *  other.m_[3][2]);
    result.m_[2][3] =  ( m_[2][0] *  other.m_[0][3] +  m_[2][1] *  other.m_[1][3] +  m_[2][2] *  other.m_[2][3] +  m_[2][3] *  other.m_[3][3]);
    result.m_[3][0] =  ( m_[3][0] *  other.m_[0][0] +  m_[3][1] *  other.m_[1][0] +  m_[3][2] *  other.m_[2][0] +  m_[3][3] *  other.m_[3][0]);
    result.m_[3][1] =  ( m_[3][0] *  other.m_[0][1] +  m_[3][1] *  other.m_[1][1] +  m_[3][2] *  other.m_[2][1] +  m_[3][3] *  other.m_[3][1]);
    result.m_[3][2] =  ( m_[3][0] *  other.m_[0][2] +  m_[3][1] *  other.m_[1][2] +  m_[3][2] *  other.m_[2][2] +  m_[3][3] *  other.m_[3][2]);
    result.m_[3][3] =  ( m_[3][0] *  other.m_[0][3] +  m_[3][1] *  other.m_[1][3] +  m_[3][2] *  other.m_[2][3] +  m_[3][3] *  other.m_[3][3]);
    return result;
}

void Matrix::transform(Vector2d &vector) const
{
	double num1 = (vector.x() *  m_[0][0] + vector.y() * m_[1][0]) + m_[3][0];
	double num2 = (vector.x() * m_[0][1] + vector.y() * m_[1][1]) + m_[3][1];
	vector.x() = num1;
	vector.y() = num2;
}