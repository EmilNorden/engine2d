#ifndef HOOK_PROVIDER_H_
#define HOOK_PROVIDER_H_

#include <vector>
#include <tuple>
#include <functional>

struct lua_State;

typedef std::tuple<std::string, std::function<int(lua_State*)>, void*> LuaHook;
//typedef std::pair<std::string, std::function<int(lua_State*)>> LuaHook;

class HookProvider 
{
public:
	virtual std::vector<LuaHook> getHooks();
};

#endif