#include "scriptengine.h"
#include "gl_renderer.h"
#include "world.h"
#include "contentloader.h"
#include "luastate.h"
#include "sdl/SDL_assert.h"
#include <lua.hpp>
#include <iostream>

static GLRenderer *renderer;
static World *world;
static ContentLoader *loader;

//int lua_print(lua_State *L);
//int lua_err(lua_State *L);

inline bool validateArgumentCount(const char *function, int actual, int expected)
{
	if(actual != expected)
	{
		std::cerr << "Invalid call to " << function << ". Expected " << expected << " arguments, got " << actual << "." << std::endl;

		return false;
	}

	return true;
}

void ScriptEngine::init(GLRenderer *rend, World *w, ContentLoader *c)
{
	renderer = rend;
	world = w;
	loader = c;
}

void ScriptEngine::registerHooks(HookProvider &provider)
{
	auto hooks = provider.getHooks();
	for(auto it = hooks.begin(); it != hooks.end(); ++it)
		registerHook(std::get<0>(*it), std::get<1>(*it), std::get<2>(*it));
	
}

void ScriptEngine::registerHook(const std::string &name, std::function<int(lua_State*)> &func, void *p)
{
	SDL_assert(func.target<lua_CFunction>() != nullptr);
	hooks_.insert(std::pair<std::string, std::pair<std::function<int(lua_State*)>, void*>>(name, std::pair<std::function<int(lua_State*)>, void*>(func, p)));
	//hooks_.insert(std::pair<std::string&, std::function<int(lua_State*)>>(name, func));
}

void setfield (lua_State *L, const char *index, void* value) {
      lua_pushstring(L, index);
	  lua_pushlightuserdata(L, value);
      lua_settable(L, -3);
    }

void* getfield (lua_State *L, const char *key) {
      void* result;
      lua_pushstring(L, key);
      lua_gettable(L, -2);  /* get background[key] */

      result = (void*)lua_topointer(L, -1);;
      lua_pop(L, 1);  /* remove number */
      return result;
    }

void ScriptEngine::hookScript(LuaState &state)
{
	for(auto pair = hooks_.cbegin(); pair != hooks_.cend(); ++pair)
	{
		lua_register(state.state(), pair->first.c_str(), *pair->second.first.target<lua_CFunction>());
		lua_getglobal(state.state(), "systemInstances");

		if(lua_istable(state.state(), -1))
		{
			setfield(state.state(), pair->first.c_str(), pair->second.second);
		}
		else
		{
			std::cerr << "Script does not implement systemInstances table" << std::endl;
		}
	}
}

//int lua_err(lua_State *L)
//{
//	int argc = lua_gettop(L);
//
//	const char *msg = lua_tostring(L, 1);
//	std::cerr << "Lua error: " << msg << std::endl;
//
//	return 0;
//}
//
//int lua_print(lua_State *L)
//{
//	int argc = lua_gettop(L);
//
//    for (int i=1; i <= argc; i++)
//	{
//        if (lua_isstring(L, i))
//			std::cout << lua_tostring(L, i) << std::endl;
//        else if(lua_isboolean(L, i))
//			std::cout << static_cast<bool>(lua_toboolean(L, i)) << std::endl;
//		else if(lua_isnumber(L, i))
//			std::cout << static_cast<double>(lua_tonumber(L, i)) << std::endl;
//    }
//
//    return 0;
//}
