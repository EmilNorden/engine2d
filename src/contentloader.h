#ifndef CONTENTLOADER_H_
#define CONTENTLOADER_H_

#include <memory>
#include <string>

#include "weakcache.h"
#include "texture.h"
#include "renderer.h"
#include "stringdescriptor.h"

class Font;
class LuaState;

class ContentLoader
{
private:
	WeakCache<std::string, Texture>	texCache_;
	WeakCache<StringDescriptor, Texture>	stringCache_;
	WeakCache<std::string, Font>	fontCache_;

	std::string			contentFolder_;
	
	std::shared_ptr<Texture> errorTexture_;
public:
	ContentLoader();
	
	void						init(const std::string &folder);
	std::shared_ptr<Texture>	loadTexture(const std::string &name);
	std::shared_ptr<Texture>	loadString(const StringDescriptor &desc);
	std::shared_ptr<Font>		loadFont(const std::string &name, size_t size);
	std::shared_ptr<LuaState>	loadScript(const std::string &name);
};

#endif