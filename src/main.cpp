﻿#include "enginecore.h"

int main(int argc, char **argv)
{
	EngineCore engine;
	engine.run();

	return 0;
}