#ifndef COLOR_H_
#define COLOR_H_

class Color
{
public:
	float a;
	float r;
	float g;
	float b;

	Color()
		: a(1), r(0), g(0), b(0)
	{
	}

	Color(float ax, float rx, float gx, float bx)
		: a(ax), r(rx), g(gx), b(bx)
	{}
};

#endif