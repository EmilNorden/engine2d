#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <iosfwd>
#include <vector>
#include <memory>
#include <string>

#include <sstream>
#include <array>
#include "textbox.h"
#include "scrollbar.h"
#include "color.h"

class GameTime;
class GLRenderer;
class Viewport;
class ContentLoader;
class EventRouter;
class Font;
class TextSprite;

class Console
{
private:
	std::vector<std::unique_ptr<TextSprite>> lineBuffer_;
	int bufferSize_;
	int consoleHeight_;
	int lineHeight_;
	int charWidth_;
	int maxLines_;
	float alpha_;
	bool visible_;
	int currentBufferIndex_;
	bool bufferFilled_;
	std::shared_ptr<Font> consoleFont_;
	std::ostringstream coutStream_;
	std::ostringstream cerrStream_;
	std::streambuf *originalCout_;
	std::streambuf *originalCerr_;
	TextBox input_;
	Scrollbar scrollbar_;

	void add(const std::string &text, const Color &color);
	//void addError(const std::string &text);
	void redirectStdStreams();
	void restoreStdStreams();

public:
	Console();
	void show();
	void hide();
	void toggleVisibility();

	void init(const Viewport &viewport, ContentLoader &content, EventRouter &events, int bufferSize_ = 0);
	void shutdown();
	void render(const Viewport &viewport, const GLRenderer &renderer);

	float alpha() const { return alpha_; }
	void setAlpha(float alpha);

	void update(const GameTime &time);
};

#endif
