#include "eventrouter.h"
#include "enginecore.h"
#include "memory.h"
#include <lua.hpp>
#include <array>
#include <stdexcept>
#include <boost/format.hpp>
#include <stdexcept>
#include "eventparams.h"
#include "luastate.h"
EventRouter *events;

 EventRouter::EventRouter()
 {
	 events = this;
 }

void EventRouter::add(const SDL_EventType &type, EventCallbackPtr &callback)
{
	 callbacks_[type].push_back(std::move(callback));
}

void EventRouter::add(const SDL_EventType &type, EventCallbackPtr &&callback)
{
	callbacks_[type].push_back(std::move(callback));
}

 void EventRouter::remove(const SDL_EventType &type, EventCallbackPtr &callback)
 {
	 for(auto it = callbacks_[type].begin(); it != callbacks_[type].end(); ++it)
	 {
		 if(*(*it) == *callback)
		 {
			 it = callbacks_[type].erase(it);
			 return;
		 }
	 }
 }

 void EventRouter::remove(const SDL_EventType &type, EventCallbackPtr &&callback)
 {
	 for(auto it = callbacks_[type].begin(); it != callbacks_[type].end(); ++it)
	 {
		 if(*(*it) == *callback)
		 {
			 it = callbacks_[type].erase(it);
			 return;
		 }
	 }
 }

std::unique_ptr<LuaInteropObject> EventRouter::getEventInteropObject(const SDL_Event &e)
 {
	 switch(e.type)
	{
		case SDL_MOUSEBUTTONDOWN:
			return std::unique_ptr<LuaInteropObject>(new MouseClickEventParam(e.button.x, e.button.y, e.button.button));
		break;
		case SDL_MOUSEMOTION:
			return std::unique_ptr<LuaInteropObject>(new MouseMoveEventParam(e.motion.x, e.motion.y));
		break;
		case SDL_QUIT:
			return LuaInteropObjectPtr(new ApplicationQuitEventParam);
		break;
		default:
			throw std::runtime_error("getEventInteropObject: Unknown event type");
		break;
	}
 }



void EventRouter::update()
{
	SDL_Event e;
		while(SDL_PollEvent(&e))
		{
			for(auto &item : callbacks_[static_cast<SDL_EventType>(e.type)])
			{
				auto interopObject = getEventInteropObject(e);
				// TODO: translate the SDL_Event to the correct LuaInteropObject
				item->call(interopObject);
			}
		}
}

SDL_EventType eventFromString(const std::string &in)
{
	if(in == "mouseclick")
		return SDL_MOUSEBUTTONDOWN;
	else if(in == "mousemove")
		return SDL_MOUSEMOTION;

 	throw std::runtime_error((boost::format("EventRouter::eventFromString: Unknown event '%1'.") % in).str().c_str());

}

int addEventRouting(lua_State *L)
{
	int argc = lua_gettop(L);

	std::string e = lua_tostring(L, 1);
	std::string luaFunc = lua_tostring(L, 2);
	
	std::string entity = lua_tostring(L, 3);
	
	SDL_EventType sdlEvent = eventFromString(e);

	events->add(sdlEvent, EventCallbackPtr(new ScriptCallback<const LuaInteropObjectPtr&>(LuaState::makeShared(L), luaFunc)));

	return 0;
}

int removeEventRouting(lua_State *L)
{
	int argc = lua_gettop(L);

	std::string e = lua_tostring(L, 1);
	std::string luaFunc = lua_tostring(L, 2);
	
	std::string entity = lua_tostring(L, 3);
	
	SDL_EventType sdlEvent = eventFromString(e);

	events->remove(sdlEvent, EventCallbackPtr(new ScriptCallback<const LuaInteropObjectPtr&>(LuaState::makeShared(L), luaFunc)));

	return 0;
}

std::vector<LuaHook> EventRouter::getHooks()
{
	std::array<LuaHook, 2> funcs = {
		LuaHook("addEventRouting", addEventRouting, this),
		LuaHook("removeEventRouting", removeEventRouting, this)
	};

	return std::vector<LuaHook>(funcs.begin(), funcs.end());
}
