#include "luastate.h"
#include "luatable.h"
#include "script_commands.h"
#include <lua.hpp>
#include <sstream>
#include <iostream>

std::map<lua_State*, std::weak_ptr<LuaState>> LuaState::stateLookup_;


LuaException::LuaException(const LuaError &error, const std::string &luaMessage)
	: error_(error), luaMessage_(luaMessage)
{
	std::stringstream ss;
	ss << "Error: " << LuaState::getErrorText(error_) << ". " << luaMessage_;
	message_ = ss.str();
}

const char *LuaException::what() const noexcept (true)
{
	return message_.c_str();
}

//LuaState::LuaState(lua_State *state)
//	: state_(state)
//{
//}

LuaState::LuaState()
	: state_(luaL_newstate())//, error_(LuaError::None)
{
	luaL_openlibs(state_);
	setupScriptHooks(*this);
	//luaopen_io(state_);
}

LuaState::LuaState(lua_State *L)
	: state_(L)
{
}

LuaState::~LuaState()
{
	try
	{
		//pcall("cleanup");
	}
	catch(LuaException &e)
	{
		std::cerr << e.what();
	}
	lua_close(state_);
}

void LuaState::inspectErrors(int error)
{
	if(error != 0)
	{
		const char *message = lua_tostring(state_, -1);
		throw LuaException(static_cast<LuaError>(error), message);
	}
}

void LuaState::open(const std::string &file, void*p)
{
	inspectErrors(luaL_dofile(state_, file.c_str()));

	/*lua_getglobal(state_, "dostuff");
	lua_pushlightuserdata(state_, p);
	lua_call(state_, 1, 1);
	int sum = (int)lua_tointeger(state_, -1);
	lua_pop(state_, 1);*/
}

//const char * luaReader(lua_State *L,
//                                    void *data,
//                                    size_t *size)
//{
//	std::stringstream *stream = reinterpret_cast<std::stringstream*>(data);
//	std::string line;
//	if(std::getline(*stream, line))
//	{
//		*size = line.size();
//		return line.c_str();
//	}
//	else
//	{
//		return nullptr;
//	}
//}

void LuaState::call(const std::string &func)
{
	lua_getglobal(state_, func.c_str());
 	lua_call(state_, 0, 0);
}

void LuaState::pcall(const std::string &func)
{
	lua_getglobal(state_, func.c_str());
	inspectErrors(lua_pcall(state_, 0, 0, 0)); // 0 arguments, 0 returnvalues, errfunc=0 (error message pushed onto stack)
}

void LuaState::dostring(const std::string &script)
{
	inspectErrors(luaL_dostring(state_, script.c_str()));
}

void LuaState::pushTable(LuaTable &table)
{
	lua_createtable(state_, 0, table.size());
	for(auto it = table.cbegin(); it != table.cend(); ++it)
	{
		lua_pushstring(state_, it->first.c_str());
		lua_pushnumber(state_, it->second);
		lua_settable(state_, -3);
/*		lua_pushnumber(state_, it->second);
		lua_setfield(state_, -2, it->first.c_str());*/
	}
	lua_setglobal(state_, "foo");
}

bool LuaState::operator==(const LuaState &other)
{
	return state_ == other.state_;
}

std::shared_ptr<LuaState> LuaState::insertShared(lua_State *L)
{
	std::shared_ptr<LuaState> state(new LuaState(L));
	stateLookup_.insert(std::pair<lua_State*, std::weak_ptr<LuaState>>(state->state(), std::weak_ptr<LuaState>(state)));

	return state;
}

std::shared_ptr<LuaState> LuaState::makeShared(lua_State *L)
{
	std::shared_ptr<LuaState> state;
	auto it = stateLookup_.find(L);
	if(it == stateLookup_.end())
	{
		state = insertShared(L);
	}
	else if(!(state = it->second.lock()))
	{
		stateLookup_.erase(it);
		state = insertShared(L);
	}

	return state;
}

std::shared_ptr<LuaState> LuaState::makeShared()
{
	std::shared_ptr<LuaState> state(new LuaState);

	stateLookup_.insert(std::pair<lua_State*, std::weak_ptr<LuaState>>(state->state(), std::weak_ptr<LuaState>(state)));
	
	return state;
}

//void LuaState::load(const std::string &script)
//{
//	std::string readerScript = script;
//	std::stringstream stream(readerScript);
//	inspectErrors(lua_load(state_, luaReader, &stream, "load"));
//}

std::string LuaState::getErrorText(const LuaError &err)
{
	switch(err)
	{
	case LuaError::None:
		return "None";
		break;
	case LuaError::ERRERR:
		return "LUA_ERRERR";
		break;
	case LuaError::ERRMEM:
		return "LUA_ERRMEM";
		break;
	case LuaError::ERRRUN:
		return "LUA_ERRRUN";
		break;
	case LuaError::ERRSYNTAX:
		return "LUA_ERRSYNTAX";
		break;
	case LuaError::YIELD:
		return "LUA_YIELD";
		break;
	default:
		std::stringstream ss;
		ss << "[Unknown return value: " << static_cast<int>(err) << "]";
		return ss.str();
	}
}
