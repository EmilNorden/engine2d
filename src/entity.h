#ifndef Entity_H_
#define Entity_H_

#include "transformation.h"
#include "obb.h"
#include "sprite.h"
#include <memory>
#include <string>

class GameTime;
class GLRenderer;

class Entity
{
private:
	std::string name_;
	std::unique_ptr<Sprite> sprite_;
	OBB bounds_;
	size_t renderedFrame_;
	size_t zindex_;
	size_t currentLayer_;
	size_t identifier_;

	bool isSolid_;
	double pushable_;
public:
	Vector2d offset;
	Entity(size_t identifier, std::unique_ptr<Sprite> &sprite);
	void updateTransformation(size_t frame);
	void draw(const GameTime &gameTime, const GLRenderer &renderer);
	
	void setRenderedFrame(size_t frame) { renderedFrame_ = frame; }
	size_t renderedFrame() const { return renderedFrame_; }

	size_t zindex() const { return zindex_; }

	OBB &bounds() { return bounds_; }

	std::unique_ptr<Sprite> &sprite() { return sprite_; }

	size_t currentLayer() const { return currentLayer_; }
	void setCurrentLayer(size_t layer) { currentLayer_ = layer; }

	Transformation &transformation();

	size_t identifier() const { return identifier_; }

	bool isSolid() const { return isSolid_; }
	void setIsSolid(bool value) { isSolid_ = value; }
	
	double pushable() const {return pushable_; }
	void setPushable(double value) { pushable_ = value;}

	std::string name() { return name_; }
	void setName(const std::string &name) { name_ = name; }

	Vector2d dimensions();
};

typedef std::shared_ptr<Entity> EntityPtr;

#endif
