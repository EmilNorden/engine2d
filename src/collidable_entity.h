#ifndef COLLIDABLEENTITY_H_
#define COLLIDABLEENTITY_H_
//
//#include "obb.h"
//
//
//#include "entity.h"
//#include <memory>
//#include "matrix.h"
//
//class CollidableEntity : public Entity
//{
//private:
//	Matrix world_;
//
//	double xScale_;
//	double yScale_;
//	double xRotation_;
//	double yRotation_;
//	double zRotation_;
//
//	size_t renderedFrame_;
//	OBB bounds_;
//
//	bool worldIsDirty_;
//	Vector2d delta_;
//
//	void buildWorld();
//public:
//	CollidableEntity(size_t identifier, const std::shared_ptr<Texture> &tex)
//		: Entity(identifier, tex), bounds_(Vector2d(0, 0), Vector2d(tex->width(), 0), Vector2d(tex->width(), tex->height()), Vector2d(0, tex->height())), renderedFrame_(0), xRotation_(0), yRotation_(0), zRotation_(0), xScale_(1), yScale_(1), worldIsDirty_(true)
//	{
//	}
//
//	CollidableEntity(size_t identifier, const std::shared_ptr<Texture> &tex, const OBB &bounds)
//		: Entity(identifier, tex), bounds_(bounds), renderedFrame_(0), xRotation_(0), yRotation_(0), zRotation_(0), xScale_(1), yScale_(1), worldIsDirty_(true)
//	{
//	}
//
//	void draw(const GameTime &gameTime, const GLRenderer &renderer);
//
//	OBB& bounds() { return bounds_; }
//
//	size_t renderedFrame() const { return renderedFrame_; }
//	void setRenderedFrame(size_t frame) { renderedFrame_ = frame; }
//
//	double xRotation() const { return xRotation_; }
//	double yRotation() const { return yRotation_; }
//	double zRotation() const { return zRotation_; }
//
//	void setXRotation(double rot) { xRotation_ = rot; }
//	void setYRotation(double rot) { yRotation_ = rot; }
//	void setZRotation(double rot) { zRotation_ = rot; }
//
//	double xScale() const { return xScale_; }
//	double yScale() const { return yScale_; }
//
//	void setXScale(double scale);
//	void setYScale(double scale);
//
//	void update(const GameTime &gameTime);
//
//	void setMovementDelta(const Vector2d &delta);
//};
//
//typedef std::shared_ptr<CollidableEntity> CEntityPtr;

#endif