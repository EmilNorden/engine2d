#ifndef RENDERER_H_
#define RENDERER_H_

#include <memory>

struct SDL_Renderer;
class Window;
class Texture;

class Renderer
{
private:
	SDL_Renderer *renderer_;
public:
	Renderer();
	~Renderer();
	void init(const Window &window, int index, unsigned int flags);
	void clear() const;
	void present() const;

	void renderTexture(const std::shared_ptr<Texture> &tex, int x, int y, int w, int h) const;

	SDL_Renderer *getSDLRenderer() const { return renderer_; }
};

#endif