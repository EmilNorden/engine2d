#include "console.h"
#include "gl_renderer.h"
#include "viewport.h"
#include "sdl/SDL_assert.h"
#include "contentloader.h"
#include "textsprite.h"
#include <algorithm>
#include <streambuf>
#include <iostream>

Console::Console()
	: currentBufferIndex_(0), visible_(false), bufferFilled_(false)
{
}

void Console::show() 
{
	visible_ = true;
	input_.setActive(true);
}

void Console::hide()
{
	visible_ = false;
	input_.setActive(false);
}

void Console::toggleVisibility()
{
	if(visible_)
		hide();
	else
		show();
}

void Console::setAlpha(float alpha)
{
	SDL_assert(alpha >= 0 && alpha <= 1);

	alpha_ = alpha;
}

void Console::redirectStdStreams()
{
	originalCout_ = std::cout.rdbuf();
	originalCerr_ = std::cerr.rdbuf();
	std::cout.rdbuf(coutStream_.rdbuf());
	std::cerr.rdbuf(cerrStream_.rdbuf());
}

void Console::restoreStdStreams()
{
	std::cout.rdbuf(originalCout_);
	std::cerr.rdbuf(originalCerr_);
}

void Console::init(const Viewport &viewport, ContentLoader &content, EventRouter &events, int bufferSize)
{
	// the Console assumes mono space fonts. DO NOT use fonts that are not mono spaced.
	consoleFont_ = content.loadFont("fonts/anonymous-pro/AnonymousPro.ttf", 12);

	TextSprite sprite(consoleFont_, Color(1, 1, 1, 1), "A");
	lineHeight_ = sprite.height();
	charWidth_ = sprite.width();
	
	double height = viewport.size().y() * 0.33; // base console height on 1/3 viewport height
	double rows = height / lineHeight_;
	int truncatedRows = static_cast<int>(rows); // truncate to round down to nearest even line number.
	consoleHeight_ = lineHeight_ * truncatedRows;
	maxLines_ = truncatedRows - 1; // Leave 1 row for input

	if(bufferSize == 0)
		bufferSize_ = maxLines_ * 2;
	else
		bufferSize_ = bufferSize;

	lineBuffer_.resize(bufferSize_);

	redirectStdStreams();

	input_.init(consoleFont_, events);
	input_.setPosition(Vector2d(0, maxLines_ * lineHeight_));
	input_.setSize(Vector2d(viewport.size().x(), 0));

	scrollbar_.init("consoleScrollbar", events);
	scrollbar_.setPosition(Vector2d(viewport.size().x() - 16, 0));
	scrollbar_.setSize(Vector2d(16, consoleHeight_ - lineHeight_));
	scrollbar_.setMin(0);
	scrollbar_.setMax(0);
}

void Console::shutdown()
{
	restoreStdStreams();
}

void Console::add(const std::string &text, const Color &color)
{
	lineBuffer_[currentBufferIndex_] = std::unique_ptr<TextSprite>(new TextSprite(consoleFont_, color, text));
	if(++currentBufferIndex_ == bufferSize_)
	{
		bufferFilled_ = true;
		currentBufferIndex_ = 0;
	}

	if(!bufferFilled_ && currentBufferIndex_ > maxLines_)
	{
		int remainder = currentBufferIndex_ - maxLines_;
		scrollbar_.setMax(remainder+1);
	}
	else if(bufferFilled_)
	{
		scrollbar_.setMax(bufferSize_ - maxLines_);
	}
}

//void Console::addError(const std::string &text)
//{
//	lineBuffer_[currentBufferIndex_] = std::unique_ptr<TextSprite>(new TextSprite(consoleFont_, Color(1, 1, 0, 0), text));
//	if(++currentBufferIndex_ == bufferSize_)
//		currentBufferIndex_ = 0;
//}

int modulo(int a, int b)
{ return (a%b+b)%b; }

void Console::render(const Viewport &viewport, const GLRenderer &renderer)
{
	if(visible_)
	{
		glLoadIdentity();
		
		renderer.renderColor(0, 0, 0, alpha_, 0, 0, viewport.size().x(), consoleHeight_);
		glColor4f(1, 1, 1, 1);
		int startIndex = currentBufferIndex_ - maxLines_;
		if(scrollbar_.max() != 0)
			startIndex -= scrollbar_.max() - (scrollbar_.value()+1);

		int lineNumber = 0;
		for(int i = startIndex; i < startIndex + maxLines_; ++i)
		{
			int circularIndex = modulo(i, bufferSize_);
			
			if(lineBuffer_[circularIndex])
			{
				glLoadIdentity();
				glTranslatef(0, lineNumber++ * lineHeight_, 0);
				renderer.renderTexture(lineBuffer_[circularIndex]->texture(), 0, 0, lineBuffer_[circularIndex]->width(), lineBuffer_[circularIndex]->height());
			}
		}

		input_.render(renderer);
		scrollbar_.render(renderer);

		glLoadIdentity();
		glTranslatef(viewport.size().x() - 16, 0, 0);
		renderer.renderColor(0, 0, 0, 1, 0, 0, 16, 16);

	}
}

void Console::update(const GameTime &time)
{
	// TODO: this cant be the best way to redirect cout and read the data line by line
	std::string line;
	std::istringstream icoutStream(coutStream_.str());
	while(std::getline(icoutStream, line))
		add(line, Color(1, 1, 1, 1));
	coutStream_.str("");

	std::istringstream icerrStream(cerrStream_.str());
	while(std::getline(icerrStream, line))
		add(line, Color(1, 1, 0, 0));
	cerrStream_.str("");

	if(visible_)
	{
		input_.update(time);
	}
}
