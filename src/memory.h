#ifndef MEMORY_H_
#define MEMORY_H_

#include <map>
#include <memory>

template <typename T>
class SharedPointerCache
{
private:
	// TODO: Investigate TBBs concurrent hash map later
	static std::map<T*, std::weak_ptr<T>> cache_;
public:
	static std::shared_ptr<T> get(T *ptr);
};

template <typename T>
std::map<T*, std::weak_ptr<T>> SharedPointerCache<T>::cache_;

template <typename T>
std::shared_ptr<T> SharedPointerCache<T>::get(T *ptr)
{
	std::shared_ptr<T> result;
	auto it = cache_.find(ptr);
	if(it != cache_.end())
	{
		
		if(result = it->second.lock())
			return result;
		else
			cache_.erase(it);
	}

	result = std::shared_ptr<T>(ptr);
	cache_.insert(std::pair<T*, std::weak_ptr<T>>(ptr, std::weak_ptr<T>(result)));
	return result;
}

#endif