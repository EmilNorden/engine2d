#include "eventparams.h"

LuaTable MouseMoveEventParam::buildLuaTable() const
{
	LuaTable table;
	table.add("x", x_);
	table.add("y", y_);
	return table;
}

LuaTable MouseClickEventParam::buildLuaTable() const
{
	LuaTable table;
	table.add("x", x_);
	table.add("y", y_);
	table.add("buttonIndex", buttonIndex_);
	return table;
}

LuaTable ApplicationQuitEventParam::buildLuaTable() const
{
	return LuaTable();
}
