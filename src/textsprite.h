#ifndef TEXT_SPRITE_H_
#define TEXT_SPRITE_H_

#include "font.h"
#include "sprite.h"
#include "texture.h"
#include <memory>
#include <string>

#include "color.h"

class TextSprite : public Sprite
{
private:
	std::shared_ptr<Texture> texture_;
	std::shared_ptr<Font> font_;
	std::string text_;
	Color color_;

	void buildTexture();
public:
	TextSprite(const std::shared_ptr<Font> &font, const Color &clr, const std::string &text = "");
	
	std::string text() const;
	void setText(const std::string &value);

	void draw(const GameTime &gameTime, const GLRenderer &renderer);
	int width() const;
	int height() const;

	std::shared_ptr<Texture> &texture() { return texture_; } // For now
};

#endif