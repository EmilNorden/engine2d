#include "textbox.h"
#include "sdl/SDL_events.h"
#include "textsprite.h"
#include "gl_renderer.h"
#include "luastate.h"
#include "eventrouter.h"
#include "gametime.h"
#include <lua.hpp>
#include <iostream>
#include <cstring>

TextBox::TextBox()
	: size_(Vector2d(150, 0)), caretPosition_(0), caretState_(false), inputCbkIndex_(-1), keydownCbkIndex_(-1), active_(false)
{
}

TextBox::~TextBox()
{
}

void TextBox::textInput(const LuaInteropObjectPtr &e)
{
	if(!active_)
		return;
/*
	bool update = false;
	if(e.type == SDL_TEXTINPUT)
	{
		if(text_.empty())
			text_ = e.text.text;
		else
		{
			size_t size = strlen(e.text.text);
			text_.insert(caretPosition_, e.text.text, size);
		}
		caretPosition_++;
		update = true;
	}
	else // SDL_KEYDOWN
	{
		switch(e.key.keysym.scancode)
		{
		case SDL_SCANCODE_RETURN:
			if(!text_.empty())
			{
				auto state = LuaState::makeShared();
				std::cout << text_ << std::endl;
				try
				{
					state->dostring(text_);
				}
				catch(LuaException &e)
				{
					std::cerr << e.what() << std::endl;
				}
				text_ = "";
				caretPosition_ = 0;
				update = true;
			}
			break;
		case SDL_SCANCODE_BACKSPACE:
			if(caretPosition_ > 0)
			{
				text_.erase(--caretPosition_, 1);
				if(caretPosition_ == 0)
					text_ = "";
				update = true;
			}
			break;
		case SDL_SCANCODE_LEFT:
			caretPosition_ = std::max(0, caretPosition_ - 1);
			break;
		case SDL_SCANCODE_RIGHT:
			caretPosition_ = std::min(static_cast<int>(text_.length()), caretPosition_ + 1);
			break;
		}
	}

	if(update)
	{
		if(text_.empty())
			textSprite_ = nullptr;
		else
			textSprite_ = std::move(std::unique_ptr<TextSprite>(new TextSprite(font_, Color(1, 1, 1, 1), text_)));
	}
	*/
}

void TextBox::init(const std::shared_ptr<Font> &font, EventRouter &events)
{
	font_ = font;
	TextSprite sprite(font_, Color(1, 1, 1, 1), "A");
	charHeight_ = sprite.height();
	charWidth_ = sprite.width();

	events.add(SDL_TEXTINPUT, EventCallbackPtr(new HostCallback<const LuaInteropObjectPtr&>(std::bind(&TextBox::textInput, this, std::placeholders::_1))));
	events.add(SDL_KEYDOWN, EventCallbackPtr(new HostCallback<const LuaInteropObjectPtr&>(std::bind(&TextBox::textInput, this, std::placeholders::_1))));
}

void TextBox::setActive(bool active)
{
	active_ = active;
}

uint32_t last = 0;
void TextBox::update(const GameTime &time)
{
	if(!active_)
		return;
	if(time.realTime() - last > 400)
	{
		last = time.realTime();
		caretState_ = !caretState_;
	}
}

void TextBox::render(const GLRenderer &renderer)
{
	glLoadIdentity();
	glTranslatef(position_.x(), position_.y(), 0);
	renderer.renderColor(1, 1, 1, 0.25, 0, 0, size_.x(), size_.y());

	if(textSprite_)
	{
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glLoadIdentity();
		glTranslatef(position_.x() + charWidth_, position_.y(), 0);
		renderer.renderTexture(textSprite_->texture(), 0, 0, textSprite_->width(), textSprite_->height());
	}

	if(caretState_)
	{
		glLoadIdentity();
		glTranslatef(position_.x() + charWidth_ * (caretPosition_+1), position_.y(), 0);
		renderer.renderColor(1,1,1,1, 0, 0, 1, charHeight_);
	}
}

void TextBox::setPosition(const Vector2d &pos)
{
	position_ = pos;
	bounds_.setMin(pos);
	bounds_.setMax(position_ + size_);
}

void TextBox::setSize(const Vector2d &size)
{
	size_ = size;
	size_.y() = charHeight_;
	bounds_.setMax(position_ + size_);
}
