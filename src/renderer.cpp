#include "renderer.h"
#include "SDL.h"
#include "window.h"
#include "error.h"
#include "texture.h"

#include "sdl/SDL_opengl.h"

Renderer::Renderer()
	: renderer_(nullptr)
{
}

Renderer::~Renderer()
{
	if(renderer_ == nullptr)
	{
		SDL_DestroyRenderer(renderer_);
		renderer_ = nullptr;
	}
}

void Renderer::init(const Window &window, int index, unsigned int flags)
{
	SDL_assert(renderer_ == nullptr);
	SDL_PTR_CALL(renderer_ = SDL_CreateRenderer(window.getSDLWindow(), index, flags));
}

void Renderer::clear() const
{
	SDL_RenderClear(renderer_);
}

void Renderer::present() const
{
	
	//SDL_RenderPresent(renderer_);
}

void Renderer::renderTexture(const std::shared_ptr<Texture> &tex, int x, int y, int w, int h) const
{
	glBindTexture( GL_TEXTURE_2D, tex->glTexture() );
 
	glBegin( GL_QUADS );
		//Bottom-left vertex (corner)
		glTexCoord2i( 0, 0 );
		glVertex3f( 100.f, 100.f, 0.0f );
 
		//Bottom-right vertex (corner)
		glTexCoord2i( 1, 0 );
		glVertex3f( 228.f, 100.f, 0.f );
 
		//Top-right vertex (corner)
		glTexCoord2i( 1, 1 );
		glVertex3f( 228.f, 228.f, 0.f );
 
		//Top-left vertex (corner)
		glTexCoord2i( 0, 1 );
		glVertex3f( 100.f, 228.f, 0.f );
	glEnd();
}
