#ifndef SCRIPT_COMMANDS_H_
#define SCRIPT_COMMANDS_H_

#include "color.h"
#include <memory>

class GLRenderer;
class World;
class LuaState;
struct lua_State;
class ContentLoader;

void initScriptCommands(GLRenderer *renderer, World *world, ContentLoader *c);

void setupScriptHooks(LuaState &state);

int lua_err(lua_State *L);

int lua_print(lua_State *L);

int renderSetClearColor(lua_State *L);

int renderGetClearColor(lua_State *L);

int worldGetDimensions(lua_State *L);

int worldSetScale(lua_State *L);

int worldCreateEntity(lua_State *L);

int worldDestroyEntity(lua_State *L);

int worldSetEntityPosX(lua_State *L);

int worldSetEntityPosY(lua_State *L);

int contentCreateStringTexture(lua_State *L);

int getMousePos(lua_State *L);


#endif

