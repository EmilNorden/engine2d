#include "script_commands.h"
#include "gl_renderer.h"
#include "contentloader.h"
#include "color.h"
#include "entity.h"
#include "world.h"
#include "luastate.h"
#include "sprite.h"
#include "texture.h"

#include "sdl/SDL.h"
#include <lua.hpp>
#include <iostream>

GLRenderer *renderer;
World *world;
ContentLoader *loader;

	inline bool validateArgumentCount(const char *function, int actual, int expected)
	{
	if(actual != expected)
	{
		std::cerr << "Invalid call to " << function << ". Expected " << expected << " arguments, got " << actual << "." << std::endl;

		return false;
	}

	return true;
}

void initScriptCommands(GLRenderer *rend, World *w, ContentLoader *c)
{
	renderer = rend;
	world = w;
	loader = c;
}

void setupScriptHooks(LuaState &state)
{
	lua_register(state.state(), "renderSetClearColor", renderSetClearColor);
	lua_register(state.state(), "renderGetClearColor", renderGetClearColor);
	lua_register(state.state(), "print", lua_print);
	lua_register(state.state(), "error", lua_err);
	lua_register(state.state(), "getDimensions", worldGetDimensions);
	lua_register(state.state(), "setScale", worldSetScale);
	lua_register(state.state(), "createEntity", worldCreateEntity);
	lua_register(state.state(), "destroyEntity", worldDestroyEntity);
	lua_register(state.state(), "setEntityPosX", worldSetEntityPosX);
	lua_register(state.state(), "setEntityPosY", worldSetEntityPosY);
	lua_register(state.state(), "getMousePos", getMousePos);
}

int lua_err(lua_State *L)
{
	int argc = lua_gettop(L);

	const char *msg = lua_tostring(L, 1);
	std::cerr << "Lua error: " << msg << std::endl;

	return 0;
}

int lua_print(lua_State *L)
{
	int argc = lua_gettop(L);

    for (int i=1; i <= argc; i++)
	{
        if (lua_isstring(L, i))
			std::cout << lua_tostring(L, i) << std::endl;
        else if(lua_isboolean(L, i))
			std::cout << static_cast<bool>(lua_toboolean(L, i)) << std::endl;
		else if(lua_isnumber(L, i))
			std::cout << static_cast<double>(lua_tonumber(L, i)) << std::endl;
    }

    return 0;
}

//
//int renderSetClearColor(lua_State *L)
//{
//	int argc = lua_gettop(L);
//	
//	if(validateArgumentCount("renderSetClearColor", argc, 4))
//	{
//		float a = static_cast<float>(lua_tonumber(L, 1));
//		float r = static_cast<float>(lua_tonumber(L, 2));
//		float g = static_cast<float>(lua_tonumber(L, 3));
//		float b = static_cast<float>(lua_tonumber(L, 4));
//
//		renderer->setClearColor(Color(a, r, g, b));
//	}
//
//	return 0;
//}

//int renderGetClearColor(lua_State *L)	
//{
//	int argc = lua_gettop(L);
//
//	Color clr = renderer->clearColor();
//
//	lua_pushnumber(L, clr.a);
//	lua_pushnumber(L, clr.r);
//	lua_pushnumber(L, clr.g);
//	lua_pushnumber(L, clr.b);
//
//	return 4;
//}

int worldGetDimensions(lua_State *L)
{
	int argc = lua_gettop(L);

	if(validateArgumentCount("getDimensions", argc, 1))
	{
		std::string entityName;
		if(lua_isstring(L, 1))
			entityName = lua_tostring(L, 1);

		EntityPtr entity = world->getEntity(entityName);
		if(entity)
		{
			Vector2d dim = entity->dimensions();
			lua_pushnumber(L, dim.x());
			lua_pushnumber(L, dim.y());
			return 2;
		}
		else
		{
			std::cerr << "getDimensions: Entity '" << entityName << "' not found." << std::endl;
		}
	}

	return 0;
}

int worldSetScale(lua_State *L)
{
	int argc = lua_gettop(L);
	
	if(validateArgumentCount("setScale", argc, 3))
	{
		std::string entityName;
		if(lua_isstring(L, 1))
			entityName = lua_tostring(L, 1);
		
		float x = static_cast<float>(lua_tonumber(L, 2));
		float y = static_cast<float>(lua_tonumber(L, 3));

		EntityPtr entity = world->getEntity(entityName);
		if(entity)
		{
			entity->transformation().setScale(Vector2d(x, y));
		}
		else
		{
			std::cerr << "setScale: Entity '" << entityName << "' not found." << std::endl;
		}
	}

	return 0;
}

int worldCreateEntity(lua_State *L)
{
	int argc = lua_gettop(L);

	if(validateArgumentCount("createEntity", argc, 2))
	{
		const char *name = lua_tostring(L, 1);
		const char *texture = lua_tostring(L, 2);

		auto tex = loader->loadTexture(texture);
		auto sprite = std::unique_ptr<Sprite>(new SimpleSprite(tex));
		auto entity = std::make_shared<Entity>(0, sprite);
		entity->setName(name);
		world->add(entity);
	}

	return 0;
}

int worldDestroyEntity(lua_State *L)
{
	int argc = lua_gettop(L);

	if(validateArgumentCount("destroyEntity", argc, 1))
	{
		const char *name = lua_tostring(L, 1);

		world->remove(name);
	}

	return 0;
}

int worldSetEntityPosX(lua_State *L)
{
	int argc = lua_gettop(L);
	if(validateArgumentCount("setEntityPosX", argc, 2))
	{
		std::string name = lua_tostring(L, 1);
		double x = static_cast<double>(lua_tonumber(L, 2));
		
		EntityPtr entity = world->getEntity(name);
		if(entity)
		{
			entity->transformation().setTranslation(Vector2d(x, entity->transformation().translation().y()));
		}
		else
		{
			std::cerr << "setEntityPosX: Entity '" << name << "' not found." << std::endl;
		}
	}

	return 0;
}

int worldSetEntityPosY(lua_State *L)
{
	int argc = lua_gettop(L);
	if(validateArgumentCount("setEntityPosY", argc, 2))
	{
		std::string name = lua_tostring(L, 1);
		double y = static_cast<double>(lua_tonumber(L, 2));
		
		EntityPtr entity = world->getEntity(name);
		if(entity)
		{
			entity->transformation().setTranslation(Vector2d(entity->transformation().translation().x(), y));
		}
		else
		{
			std::cerr << "setEntityPosY: Entity '" << name << "' not found." << std::endl;
		}
	}

	return 0;
}

int contentCreateStringTexture(lua_State *L)
{
	int argc = lua_gettop(L);

	if(validateArgumentCount("createStringTexture", argc, 3))
	{
		std::string name = lua_tostring(L, 1);
		std::string font = lua_tostring(L, 2);
		int size = lua_tointeger(L, 3);
		name = "$" + name;

		//auto font = loader->loadFont(name, size);
		
	}

	return 0;
}

int worldSubscibreEvent(lua_State *L)
{
	int argc = lua_gettop(L);

	if(validateArgumentCount("subscribeEvent", argc, 3))
	{
		std::string entity = lua_tostring(L, 1);
		std::string e = lua_tostring(L, 2);
		std::string callback = lua_tostring(L, 3);
	}

	return 0;
}

int getMousePos(lua_State *L)
{
	lua_gettop(L);

	int x, y;
	SDL_GetMouseState(&x, &y);
	
	lua_pushnumber(L, x);
	lua_pushnumber(L, y);
	
	return 2;	
}
//#include "script_commands.h"
//#include "gl_renderer.h"
//#include "color.h"
//#include "entity.h"
//#include "world.h"
//#include "luastate.h"
//#include "gametime.h"
//#include <lua.hpp>
//#include <iostream>
//
//GLRenderer *renderer;
//World *world;
//GameTime *gameTime;
//
//inline bool validateArgumentCount(const char *function, int actual, int expected)
//{
//	if(actual != expected)
//	{
//		std::cerr << "Invalid call to " << function << ". Expected " << expected << " arguments, got " << actual << "." << std::endl;
//
//		return false;
//	}
//
//	return true;
//}
//
//void initScriptCommands(GLRenderer *rend, World *w, GameTime *g)
//{
//	renderer = rend;
//	world = w;
//	gameTime = g;
//}
//
//void initScriptCallbacks(LuaState &state)
//{
//	lua_register(state.state(), "renderSetClearColor", renderSetClearColor);
//	lua_register(state.state(), "renderGetClearColor", renderGetClearColor);
//	lua_register(state.state(), "print", lua_print);
//	lua_register(state.state(), "error", lua_err);
//	lua_register(state.state(), "setScale", worldSetScale);
//	lua_register(state.state(), "getScale", worldGetScale);
//	lua_register(state.state(), "getRealTime", getRealTime);
//}
//
//int lua_err(lua_State *L)
//{
//	int argc = lua_gettop(L);
//
//	const char *msg = lua_tostring(L, 1);
//	std::cerr << "Lua error: " << msg << std::endl;
//
//	return 0;
//}
//
//int lua_print(lua_State *L)
//{
//	int argc = lua_gettop(L);
//
//    for (int i=1; i <= argc; i++)
//	{
//        if (lua_isstring(L, i))
//			std::cout << lua_tostring(L, i) << std::endl;
//        else if(lua_isboolean(L, i))
//			std::cout << static_cast<bool>(lua_toboolean(L, i)) << std::endl;
//		else if(lua_isnumber(L, i))
//			std::cout << static_cast<double>(lua_tonumber(L, i)) << std::endl;
//    }
//
//    return 0;
//}
//
//int renderSetClearColor(lua_State *L)
//{
//	int argc = lua_gettop(L);
//	
//	if(validateArgumentCount("renderSetClearColor", argc, 4))
//	{
//		float a = static_cast<float>(lua_tonumber(L, 1));
//		float r = static_cast<float>(lua_tonumber(L, 2));
//		float g = static_cast<float>(lua_tonumber(L, 3));
//		float b = static_cast<float>(lua_tonumber(L, 4));
//
//		renderer->setClearColor(Color(a, r, g, b));
//	}
//
//	return 0;
//}
//
//int renderGetClearColor(lua_State *L)	
//{
//	int argc = lua_gettop(L);
//
//	Color clr = renderer->clearColor();
//
//	lua_pushnumber(L, clr.a);
//	lua_pushnumber(L, clr.r);
//	lua_pushnumber(L, clr.g);
//	lua_pushnumber(L, clr.b);
//
//	return 4;
//}
//
//int worldSetScale(lua_State *L)
//{
//	int argc = lua_gettop(L);
//	
//	if(validateArgumentCount("setScale", argc, 3))
//	{
//		std::string entityName;
//		if(lua_isstring(L, 1))
//			entityName = lua_tostring(L, 1);
//		
//		float x = static_cast<float>(lua_tonumber(L, 2));
//		float y = static_cast<float>(lua_tonumber(L, 3));
//
//		EntityPtr entity = world->getEntity(entityName);
//		if(entity)
//		{
//			entity->transformation().setScale(Vector2d(x, y));
//		}
//		else
//		{
//			std::cerr << "Entity '" << entityName << "' not found." << std::endl;
//		}
//	}
//
//	return 0;
//}
//
//int worldGetScale(lua_State *L)
//{
//	int argc = lua_gettop(L);
//
//	if(validateArgumentCount("getScale", argc, 1))
//	{
//		std::string entityName;
//		if(lua_isstring(L, 1))
//			entityName = lua_tostring(L, 1);
//
//		EntityPtr entity = world->getEntity(entityName);
//		if(entity)
//		{
//			lua_pushnumber(L, entity->transformation().scale().x());
//			lua_pushnumber(L, entity->transformation().scale().y());
//
//			return 2;
//		}
//		else
//		{
//			std::cerr << "Entity '" << entityName << "' not found." << std::endl;
//
//			return 0;
//		}
//	}
//}
//
//int getRealTime(lua_State *L)
//{
//	lua_pushinteger(L, gameTime->realTime());
//
//	return 1;
//}
