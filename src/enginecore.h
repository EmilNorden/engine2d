#ifndef ENGINE_CORE_H_
#define ENGINE_CORE_H_

#include "contentloader.h"
#include "gl_renderer.h"
#include "scriptengine.h"
#include "world.h"
#include "eventrouter.h"

class EngineCore
{
private:
	ContentLoader content;
	GLRenderer renderer;
	World world;
	ScriptEngine scriptEngine;
	EventRouter events;
public:

	EngineCore();

	void run();
};

#endif