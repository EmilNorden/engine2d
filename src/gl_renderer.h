#ifndef GL_RENDERER_H_
#define GL_RENDERER_H_

#include "vector2.h"
#include "color.h"
#include "sdl/SDL.h"
#include "sdl/SDL_opengl.h"
#include "hook_provider.h"
#include <memory>

class GUIText;
class Texture;
class Window;

class GLRenderer : public HookProvider
{
private:
	SDL_GLContext context_;
	Color clearColor_;
public:
	GLRenderer();
	~GLRenderer();

	void init(const Window &window);

	void clear() const;
	void present(const Window &window) const;

	Vector2i getViewportDimensions() const;

	Color clearColor() const { return clearColor_; }
	void setClearColor(const Color &color);

	void renderTexture(const std::shared_ptr<Texture> &tex, int x, int y, int w, int h) const;
	void renderTexture(const std::unique_ptr<Texture> &tex, int x, int y, int w, int h) const;
	void renderColor(float r, float g, float b, float a, int x, int y, int w, int h) const;
	void renderText(const std::shared_ptr<GUIText> &text) const;

	virtual std::vector<LuaHook> getHooks() override;
};

#endif
