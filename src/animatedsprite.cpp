#include "animatedsprite.h"
#include "gl_renderer.h"
#include "texture.h"
#include "gametime.h"
#include "sdl/SDL_assert.h"
#include <memory>
#include <cstdint>
#include <limits>

AnimationFrame::AnimationFrame(const std::shared_ptr<Texture> &texture, int duration,  const Vector2d &offset)
	: texture_(texture), duration_(duration), offset_(offset)
{
}

AnimationFrame::~AnimationFrame()
{
}

AnimatedSprite::AnimatedSprite()
	: frameTimer_(0), currentFrameIndex_(0)
{

}

AnimatedSprite::AnimatedSprite(std::vector<AnimationFrame> &frames)
	: frames_(frames), frameTimer_(0), currentFrameIndex_(0)
{
	calculateHeight();
}


void AnimatedSprite::calculateHeight()
{
	maxWidth_ = std::numeric_limits<int>::min();
	maxHeight_ = std::numeric_limits<int>::min();
	for(uint_fast32_t i = 0; i < frames_.size(); ++i)
	{
		maxWidth_ = std::max(maxWidth_, frames_[i].texture()->width());
		maxHeight_ = std::max(maxHeight_, frames_[i].texture()->height());
	}
}

void AnimatedSprite::draw(const GameTime &gameTime, const GLRenderer &renderer)
{
	SDL_assert(frames_.size() > 0);

	AnimationFrame &current = frames_[currentFrameIndex_];

	renderer.renderTexture(current.texture(),
		0,
		0,
		current.texture()->width(),
		current.texture()->height());

	frameTimer_ += gameTime.frameTime();

	if(frameTimer_ > current.duration())
	{
		frameTimer_ -= current.duration();

		currentFrameIndex_ = (currentFrameIndex_ + 1) % frames_.size();

		transformation_.setOffset(current.offset());
	}
}
