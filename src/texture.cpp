#include "texture.h"
#include "sdl/SDL.h"
#include "sdl/extensions/SDL_image.h"

Texture::Texture(SDL_Surface *surface)
{
	SDL_assert(surface != nullptr);

	nOfColors_ = surface->format->BytesPerPixel;
	width_ = surface->w;
	height_ = surface->h;
	SDL_assert(nOfColors_ == 4 || nOfColors_ == 3);

	if(nOfColors_ == 4)
	{
		if(surface->format->Rmask == 0x000000ff)
			textureFormat_ = GL_RGBA;
		else
			textureFormat_ = GL_BGRA;
	}
	else if(nOfColors_ == 3)
	{
		if(surface->format->Rmask == 0x000000ff)
			textureFormat_ = GL_RGB;
		else
			textureFormat_ = GL_BGR;
	}

	glGenTextures(1, &texture_);
	glBindTexture(GL_TEXTURE_2D, texture_);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, nOfColors_, surface->w, surface->h, 0, textureFormat_, GL_UNSIGNED_BYTE, surface->pixels);
}

Texture::~Texture()
{
	glDeleteTextures( 1, &texture_ );
}