#include "error.h"
#include "sdl/SDL.h"
#include "sdl/extensions/SDL_ttf.h"
#include <iostream>
#include <sstream>

void reportErrorAndDie(const char *file, int line, const char *message)
{
	if(SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", message, NULL) == -1)
	{
		// stderr fallback
		std::cerr << "An error occured in " << file << ": " << line;
	}

	exit(1);
}

void sdlError(const char *file, int line)
{
	std::stringstream message;
	message << "An error occured in " << file << ":" << line << std::endl << "Message: " << SDL_GetError();
	reportErrorAndDie(file, line, message.str().c_str());
}

void ttfError(const char *file, int line)
{
	std::stringstream message;
	message << "An error occured in " << file << ":" << line << std::endl << "Message: " << TTF_GetError();
	reportErrorAndDie(file, line, message.str().c_str());
}