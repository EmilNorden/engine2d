#ifndef FONT_H_
#define FONT_H_

#include "sdl/extensions/SDL_ttf.h"
#include <string>

class Font
{
private:
	TTF_Font *font_;
	std::string identifier_;
public:
	Font(TTF_Font* font, const std::string &identifier);
	~Font();

	std::string name() const;

	TTF_Font *getSDLFont() const { return font_; }
};

#endif