#include "scrollbar.h"
#include "gl_renderer.h"
#include "eventrouter.h"
#include "eventparams.h"
#include <string>

Scrollbar::Scrollbar()
	: min_(0), max_(100), value_(0), size_(Vector2d(16, 100)), visible_(false)
{
}

Scrollbar::~Scrollbar()
{
}

void Scrollbar::mouseClick(const LuaInteropObjectPtr &e)
{
	const MouseClickEventParam& clickParam = (const MouseClickEventParam&)e;
	// TODO: Implement click eventparam and fix this
	Vector2d coordinate(0, 0); //(e.button.x, e.button.y);

	if(value_ > min_ && minButtonBounds_.contains(coordinate))
		value_--;
	else if(value_ < max_-1 && maxButtonBounds_.contains(coordinate))
		value_++;
}

void Scrollbar::init(const std::string &name, EventRouter &events)
{
	name_ = name;
	events.add(SDL_MOUSEBUTTONDOWN, EventCallbackPtr(new HostCallback<const LuaInteropObjectPtr&>(std::bind(&Scrollbar::mouseClick, this, std::placeholders::_1))));
}

void Scrollbar::render(const GLRenderer &renderer)
{
	if(visible_)
	{
		/* Top button */
		glLoadIdentity();
		glTranslatef(position_.x(), position_.y(), 0);
		renderer.renderColor(0, 0, 0, 1, 0, 0, size_.x(), size_.x());

		/* Scroll area */
		glLoadIdentity();
		glTranslatef(position_.x(), position_.y() + size_.x(), 0);
		renderer.renderColor(0.5, 0.5, 0.5, 1, 0, 0, size_.x(), scrollAreaHeight_);

		/* Bottom button */
		glLoadIdentity();
		glTranslatef(position_.x(), position_.y() + size_.y() - size_.x(), 0);
		renderer.renderColor(0, 0, 0, 1, 0, 0, size_.x(), size_.x());

		/* Scroll rectangle */
		glLoadIdentity();
		glTranslatef(position_.x(), position_.y() + size_.x() + (scrollHeight_ * value_), 0);
		renderer.renderColor(0.8, 0.8, 0.8, 1, 0, 0, 16, scrollHeight_);
	}
}

void Scrollbar::calculateScrollHeight()
{
	int range = max_ - min_;
	scrollHeight_ = static_cast<float>(scrollAreaHeight_) / range;
}

void Scrollbar::calculateButtonBounds()
{
	minButtonBounds_.setMin(position_);
	minButtonBounds_.setMax(position_ + Vector2d(size_.x(), size_.x()));

	maxButtonBounds_.setMin(position_ + Vector2d(0, scrollAreaHeight_ + size_.x()));
	maxButtonBounds_.setMax(maxButtonBounds_.minvec() + Vector2d(size_.x(), size_.x()));
}

void Scrollbar::setMin(int min)
{
	if(min_ != min)
	{
		min_ = min;
		 
		calculateScrollHeight();
	}
}

void Scrollbar::setMax(int max)
{
	if(max_ != max)
	{
		if(value_ >= max_ - 1)
			value_ = max - 1;
		max_ = max;

		calculateScrollHeight();
	}
}

void Scrollbar::setSize(const Vector2d &size)
{
	size_ = size;

	scrollAreaHeight_ = size_.y() - (size_.x() + size_.x());
	calculateScrollHeight();
	calculateButtonBounds();
}

void Scrollbar::setPosition(const Vector2d &pos)
{
	position_ = pos;

	calculateButtonBounds();
}
