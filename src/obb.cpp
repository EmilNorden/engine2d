#define NOMINMAX
//#include <Windows.h>
#include "GL/gl.h"

#include "obb.h"
#include "aabb.h"
#include "transformation.h"
#include "mtv.h"
#include <cfloat>
#include <algorithm>

#include <iostream>

OBB::OBB(const Vector2d &v1, const Vector2d &v2, const Vector2d &v3, const Vector2d &v4)
{
	corners_[0] = v1;
	corners_[1] = v2;
	corners_[2] = v3;
	corners_[3] = v4;

	recalcNormals();
}

OBB::OBB(const AABB &aabb)
{
	corners_[0] = aabb.minvec();
	corners_[1] = Vector2d(aabb.minvec().x(), aabb.maxvec().y());
	corners_[2] = aabb.maxvec();
	corners_[3] = Vector2d(aabb.maxvec().x(), aabb.minvec().y());

	recalcAxisAlignedNormals();
}

void OBB::recalcNormals()
{
	Vector2d edges[4] = 
	{
		transformedCorners_[1] - transformedCorners_[0],
		transformedCorners_[2] - transformedCorners_[1],
		transformedCorners_[3] - transformedCorners_[2],
		transformedCorners_[0] - transformedCorners_[3]
	};

	for(int i = 0; i < 4; ++i)
	{
		normals_[i] = Vector2d(edges[i].y(), -edges[i].x());
		normals_[i].normalize();
	}

	for(int i= 0; i < 4; ++i)
	{
		auto normalizedEdge = edges[i];
		auto edgeLength = edges[i].length();
		normalizedEdge.normalize();
		normalizedEdge = normalizedEdge * (edgeLength / 2.0); 
		auto edgeCenter = transformedCorners_[i] + normalizedEdge;

		normalLines_[i][0] = edgeCenter;
		Vector2d n = normals_[i];
		n.normalize();
		normalLines_[i][1] = edgeCenter + (n * -30);
	}
}

void OBB::recalcAxisAlignedNormals()
{
	transformedCorners_[0] = corners_[0];
	transformedCorners_[1] = corners_[1];
	transformedCorners_[2] = corners_[2];
	transformedCorners_[3] = corners_[3];

	normals_[0] = Vector2d(-1, 0);
	normals_[1] = Vector2d(0, 1);
	normals_[2] = Vector2d(1, 0);
	normals_[3] = Vector2d(0, -1);

	Vector2d edges[4] = 
	{
		transformedCorners_[1] - transformedCorners_[0],
		transformedCorners_[2] - transformedCorners_[1],
		transformedCorners_[3] - transformedCorners_[2],
		transformedCorners_[0] - transformedCorners_[3]
	};

	for(int i= 0; i < 4; ++i)
	{
		auto normalizedEdge = edges[i];
		auto edgeLength = edges[i].length();
		normalizedEdge.normalize();
		normalizedEdge = normalizedEdge * (edgeLength / 2.0); 
		auto edgeCenter = transformedCorners_[i] + normalizedEdge;

		normalLines_[i][0] = edgeCenter;
		Vector2d n = normals_[i];
		n.normalize();
		normalLines_[i][1] = edgeCenter + (n * -30);
	}
}

bool OBB::intersectsInternal(OBB &other)
{
	for(int i = 0; i < 4; ++i)
	{
		Vector2d range1, range2;
		findMinMaxIn1DProjection(normals_[i], range1.x(), range1.y());
		other.findMinMaxIn1DProjection(normals_[i], range2.x(), range2.y());

		if(!rangeIntersects(range1, range2))
			return false;
	}

	return true;
}
bool b = false;

bool OBB::intersects(OBB &other)
{
	b = intersectsInternal(other) && other.intersectsInternal(*this);
	return b;
}

double OBB::getOverlap(const Vector2d &range1, const Vector2d &range2)
{
	auto overlap = std::min(range1.y(), range2.y()) - std::max(range1.x(), range2.x());

	return overlap;
}

bool OBB::intersects(OBB &other, MTV&mtv)
{
	Vector2d mtvAxis;
	double mtvOverlap = DBL_MAX;
	for(int i = 0; i < 4; ++i)
	{
		Vector2d range1, range2;
		findMinMaxIn1DProjection(normals_[i], range1.x(), range1.y());
		other.findMinMaxIn1DProjection(normals_[i], range2.x(), range2.y());

		if(!rangeIntersects(range1, range2))
		{
			return false;
		}
		else
		{
			double overlap = getOverlap(range1, range2);
			if(overlap < mtvOverlap)
			{
				mtvOverlap = overlap;
				mtvAxis = normals_[i];
			}
		}
	}

	for(int i = 0; i < 4; ++i)
	{
		Vector2d range1, range2;
		findMinMaxIn1DProjection(other.normals_[i], range1.x(), range1.y());
		other.findMinMaxIn1DProjection(other.normals_[i], range2.x(), range2.y());

		if(!rangeIntersects(range1, range2))
		{
			return false;
		}
		else
		{
			double overlap = getOverlap(range1, range2);

			if(overlap < mtvOverlap)
			{
				mtvOverlap = overlap;
				mtvAxis = other.normals_[i];
			}
		}
	}

	mtv.setAxis(mtvAxis);
	mtv.setOverlap(mtvOverlap);

	return true;
}

void OBB::findMinMaxIn1DProjection(const Vector2d &axis, double &min, double &max) const
{
	min = DBL_MAX;
	max = -DBL_MAX;
	for(int i = 0; i < 4; ++i)
	{
		double dot = transformedCorners_[i].dot(axis);

		if(dot < min)
			min = dot;
		if(dot > max)
			max = dot;
	}
}

bool OBB::rangeIntersects(const Vector2d &firstRange, const Vector2d &secondRange) const
{
	return (firstRange.x() >= secondRange.x() && firstRange.x() <= secondRange.y()) || (firstRange.y() <= secondRange.y() && firstRange.y() >= secondRange.x()) ||
		(secondRange.x() >= firstRange.x() && firstRange.x() <= firstRange.y()) || (secondRange.y() <= firstRange.y() && secondRange.y() >= firstRange.x());
}

void OBB::render() 
{
	glLineWidth(1.0);

	glDisable(GL_TEXTURE_2D);

	//glLoadIdentity();

	glBegin(GL_LINE_LOOP);
	if(b)
		glColor3f(1.0f, 0.0f, 0.0f);
	else
		glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2dv(transformedCorners_[0].arr());
	glVertex2dv(transformedCorners_[1].arr());
	glVertex2dv(transformedCorners_[2].arr());
	glVertex2dv(transformedCorners_[3].arr());
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	for(int i = 0; i < 4; ++i)
	{
		glVertex2dv(normalLines_[i][0].arr());
		glVertex2dv(normalLines_[i][1].arr());
	}
	glEnd();
	//glFlush();

	
	

}

bool OBB::intersects(const AABB &other)
{
	OBB temp(other);

	return intersects(temp);
	//return other.intersects(*this);
}

void OBB::updateTransformation(const Transformation &transf)
{
	for(int i = 0; i < 4; ++i)
	{
		transformedCorners_[i] = corners_[i];
		transf.world().transform(transformedCorners_[i]);
	}

	recalcNormals();
}
