#include "entity.h"
#include "gl_renderer.h"
#include "sprite.h"
#include "GL/gl.h"

Entity::Entity(size_t identifier, std::unique_ptr<Sprite> &sprite)
	: 
	identifier_(identifier),
	isSolid_(false),
	sprite_(std::move(sprite)),
	bounds_(Vector2d(0, 0),
	Vector2d(0, sprite_->height()),
	Vector2d(sprite_->width(),
	sprite_->height()),
	Vector2d(sprite_->width(), 0)),
	renderedFrame_(0),
	zindex_(0)
{
}

void Entity::updateTransformation(size_t frame)
{
	if(sprite_->transformation().applyChanges(frame))
		bounds_.updateTransformation(sprite_->transformation());
}

void Entity::draw(const GameTime &gameTime, const GLRenderer &renderer)
{
	bounds_.render();
	//glLoadIdentity();
	glMultMatrixd(sprite_->transformation().world().ptr());	
	//glTranslated(offset.x(), offset.y(), 0);
	glColor3f(1, 1, 1);
	sprite_->draw(gameTime, renderer);
}

Transformation &Entity::transformation() { return sprite_->transformation(); }

Vector2d Entity::dimensions()
{
	int spriteHeight = sprite_->height();
	int spriteWidth = sprite_->width();
	double scaledHeight = spriteHeight * transformation().scale().y();
	double scaledWidth = spriteWidth * transformation().scale().x();

	return Vector2d(scaledWidth, scaledHeight);	
}
