#ifndef MATRIX_H_
#define MATRIX_H_

#include "vector2.h"

class Matrix
{
private:
	double m_[4][4];

public:
	Matrix();
	static Matrix createTranslation(const Vector2d &translation);
	static Matrix createScale(const Vector2d &scale);
	static Matrix createRotation(const Vector2d &rotation);

	void transform(Vector2d &vector) const;

	const double *ptr() const { return &m_[0][0]; }

	Matrix operator*(const Matrix &other);
};

#endif