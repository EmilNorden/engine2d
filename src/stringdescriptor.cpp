#include "stringdescriptor.h"

StringDescriptor::StringDescriptor(const std::string &t, size_t s, const std::shared_ptr<Font> f)
		: text(t), size(s), font(f)
{
}

bool StringDescriptor::operator <(const StringDescriptor &rhs) const
	{
		if(size == rhs.size)
		{
			if(font->name() == rhs.font->name())
			{
				return text < rhs.text;
			}
			else
			{
				return font->name() < rhs.font->name();
			}
		}
		else
		{
			return size < rhs.size;
		}
	}