#include "sprite.h"
#include "gl_renderer.h"
#include "texture.h"

SimpleSprite::SimpleSprite(const std::shared_ptr<Texture> &texture)
	: texture_(texture)
{
}

void SimpleSprite::draw(const GameTime &gameTime, const GLRenderer &render)
{
	render.renderTexture(texture_, 0, 0, texture_->width(), texture_->height());
}

int SimpleSprite::width() const
{
	return texture_->width();
}

int SimpleSprite::height() const
{
	return texture_->height();
}