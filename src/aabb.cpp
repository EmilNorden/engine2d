#include "aabb.h"
#include "obb.h"
#include "sdl/SDL_assert.h"
#include <algorithm>

AABB::AABB()
{
}

AABB::~AABB()
{
}

AABB::AABB(const Vector2d &min, const Vector2d &max)
	: min_(min), max_(max)
{
	SDL_assert(max_.x() > min_.x());
	SDL_assert(max_.y() > min_.y());
}

bool AABB::intersects(const AABB &other) const
{
	return max_.x() >= other.min_.x() && min_.x() <= other.max_.x() && 
		max_.y() >= other.min_.y() && min_.y() <= other.max_.y();
}

void AABB::inflate(const AABB &other)
{
	min_.x() = std::min(min_.x(), other.min_.x());
	min_.y() = std::min(min_.y(), other.min_.y());

	max_.x() = std::max(max_.x(), other.max_.x());
	max_.y() = std::max(max_.y(), other.max_.y());
}

void AABB::inflate(const OBB &other)
{
	for(int i = 0; i < 4; ++i)
	{
		min_.x() = std::min(min_.x(), other.transformedCorners_[i].x());
		min_.y() = std::min(min_.y(), other.transformedCorners_[i].y());

		max_.x() = std::max(max_.x(), other.transformedCorners_[i].x());
		max_.y() = std::max(max_.y(), other.transformedCorners_[i].y());
	}
	
}

bool AABB::contains(const Vector2d &point) const
{
	return point.x() >= min_.x() && point.x() < max_.x() && point.y() >= min_.y() && point.y() < max_.y();
}

bool AABB::intersects(const OBB &other) const
{
	for(int i = 0; i < 4; ++i)
	{
		if(contains(other.transformedCorners_[i]))
			return true;
	}

	return false;
}
