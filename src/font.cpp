#include "font.h"


Font::Font(TTF_Font* font, const std::string &identifier)
	: font_(font), identifier_(identifier)
{
}

Font::~Font()
{
	TTF_CloseFont(font_);
}

std::string Font::name() const
{
	return identifier_;
}