#ifndef CALLBACK_H_
#define CALLBACK_H_

#include <functional>
#include <memory>
#include <string>
#include "luastate.h"
#include "luatable.h"

enum class CallbackType
	{
		Host,
		Script
	};

template <typename T>
class Callback
{
public:
	Callback(const CallbackType &type);
	CallbackType type() const;
	virtual void call(T value) = 0;
	virtual bool operator==(const Callback<T> &other) = 0;
private:
	CallbackType type_;
};

template <typename T>
Callback<T>::Callback(const CallbackType &type)
	: type_(type)
{
}

template <typename T>
CallbackType Callback<T>::type() const
{
	return type_;
}

template <typename T>
class ScriptCallback : public Callback<T>
{
private:
	std::shared_ptr<LuaState> state_; // TODO: This should be a weak pointer!
	std::string function_;
public:
	ScriptCallback(const std::shared_ptr<LuaState> &state, const std::string &function);
	void call(T value) override;

	bool operator==(const Callback<T> &other);
};

template <typename T>
ScriptCallback<T>::ScriptCallback(const std::shared_ptr<LuaState> &state, const std::string &function)
	: state_(state), function_(function), Callback<T>(CallbackType::Script)
{
}

template <typename T>
void ScriptCallback<T>::call(T value)
{
	LuaTable table = value->buildLuaTable();
	state_->pushTable(table);
	state_->call(function_);
}

template <typename T>
bool ScriptCallback<T>::operator==(const Callback<T> &other)
{
	return other.type() == CallbackType::Script &&
		static_cast<const ScriptCallback<T>&>(other).state_ == state_ &&
		static_cast<const ScriptCallback<T>&>(other).function_ == function_;
}

template <typename T>
class HostCallback : public Callback<T>
{
private:
	std::function<void(T)> function_;
public:
	HostCallback(const std::function<void(T)> &func);
	void call(T value) override;

	bool operator==(const Callback<T> &other);
};

template <typename T>
HostCallback<T>::HostCallback(const std::function<void(T)> &func)
	: function_(func), Callback<T>(CallbackType::Host)
{
}

template <typename T>
void HostCallback<T>::call(T value)
{
	function_(value);
}

template <typename T>
bool HostCallback<T>::operator==(const Callback<T> &other)
{
	return other.type() == CallbackType::Host &&
		static_cast<const HostCallback<T>&>(other).function_.template target<void(*)(T)>() == function_.template target<void(*)(T)>(); // TODO: This will probably not work
}


#endif
