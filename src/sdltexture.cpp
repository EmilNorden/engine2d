#include "sdltexture.h"

#include "sdl/SDL.h"

SDLTexture::SDLTexture(SDL_Texture *texture)
	: texture_(texture)
{
	SDL_QueryTexture(texture_, NULL, NULL, &width_, &height_);
}

SDLTexture::~SDLTexture()
{
	SDL_DestroyTexture(texture_);
}