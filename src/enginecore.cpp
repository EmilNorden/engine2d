#include "console.h"
#include "enginecore.h"
#include "error.h"
#include "eventrouter.h"
#include "sdl/SDL.h"
#include "viewport.h"
#include "window.h"	

#include "luastate.h"

#include "script_commands.h"
#include "gametime.h"

#include <iostream>
#include <cstring>

#include "textsprite.h"
#include "sprite.h"

GLRenderer *scriptRenderer;

EngineCore::EngineCore()
{
}

bool quit = false;
void handleQuit(const LuaInteropObjectPtr &e)
{
	quit = true;
}

void EngineCore::run()
{
	SDL_CALL(SDL_Init(SDL_INIT_EVERYTHING));
	TTF_CALL(TTF_Init());

	content.init("../assets");
	world.init(events);
	
	events.add(SDL_QUIT, EventCallbackPtr(new HostCallback<const LuaInteropObjectPtr&>(handleQuit)));

	Viewport viewport(Vector2d(0, 0), Vector2d(800, 600));
	Console console;
	console.init(viewport, content, events);
	console.setAlpha(0.75f);

	Window win("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	renderer.init(win);

	scriptRenderer = &renderer;

	scriptEngine.registerHooks(renderer);
	scriptEngine.registerHooks(events);


	initScriptCommands(&renderer, &world, &content);

	auto state = content.loadScript("scripts/main.lua");

	scriptEngine.hookScript(*state.get());

	try
	{
		state->pcall("main");
	}
	catch(LuaException &e)
	{
		std::cerr << e.what();
	}
	
	GameTime gameTime(SDL_GetTicks());

	size_t frame = 0;
	int x = 0;
	int y = 0;
	Uint8 *prevData = nullptr;
	while(!quit)
	{
		frame++;

		events.update();

		int num;
		const Uint8 *data = SDL_GetKeyboardState(&num);
		if(prevData == nullptr)
			prevData = new Uint8[num];

		Vector2d delta(x, y);
		if(data[SDL_SCANCODE_RIGHT])
		{
			delta.x()++;
		}
		else if(data[SDL_SCANCODE_LEFT])
		{
			delta.x()--;
		}
		else if(data[SDL_SCANCODE_UP])
		{
			delta.y()--;
		}
		else if(data[SDL_SCANCODE_DOWN])
		{
			delta.y()++;
		}

		if(data[SDL_SCANCODE_TAB] && prevData != nullptr && !prevData[SDL_SCANCODE_TAB])
			console.toggleVisibility();

		//link->sprite()->transformation().setTranslation(link->sprite()->transformation().translation() + delta * 2);
		
		gameTime.frameUpdate(SDL_GetTicks());

		auto uppb = SDL_GetTicks();

		// Update positions
		for(auto it = world.entitiesBegin(); it != world.entitiesEnd(); ++it)
			(*it)->updateTransformation(frame);

		auto uppa = SDL_GetTicks();


		auto updb = SDL_GetTicks();

		// Check collisions and re-sort entities.
		world.update(frame, gameTime);

		auto upda = SDL_GetTicks();

		auto visb = SDL_GetTicks();
		// Find visible entities in current frame.

		world.updateVisibleEntities(frame, viewport);

		auto visa = SDL_GetTicks();

		auto quadb = SDL_GetTicks();
		// Rebuild quadtree.
		world.buildQuadTree(200, 0);
		auto quada = SDL_GetTicks();
		
		// Draw
		
		auto renb = SDL_GetTicks();
		renderer.clear();

		world.drawCulled(frame, gameTime, viewport, renderer);

		console.update(gameTime);
		console.render(viewport, renderer);

		renderer.present(win);
		
		auto rena = SDL_GetTicks();

		//if(data[SDL_SCANCODE_RETURN])
		/*{		std::stringstream ss;
		ss << gameTime.frameTime() << " - " << uppa - uppb << " - " << upda - updb << " - " << visa - visb << " - " << quada - quadb << " - " << rena - renb;

		SDL_SetWindowTitle(win.getSDLWindow(), ss.str().c_str());
		
		}*/
		
		memcpy(prevData, data, num * sizeof(Uint8));
	}

	console.shutdown();

	if(prevData != nullptr)
		delete[] prevData;

	SDL_Quit();

}
