#ifndef COLLISION_HANDLER_H_
#define COLLISION_HANDLER_H_

#include "entity.h"
#include "quadtree.h"

class CollisionHandler
{
private:
	void checkCollision(EntityPtr a, EntityPtr b);
public:
	template <typename Iter>
	void runPass(size_t frame, Iter a, Iter b, typename Iter::iterator_category *p = 0);

	void collisionCheck(size_t frame, std::vector<EntityPtr> &entities, QuadTree<EntityPtr> &entityTree);
};



template <typename Iter>
void CollisionHandler::runPass(size_t frame, Iter a, Iter b, typename Iter::iterator_category *p)
{
	for(auto it1 = a; it1 != b; ++it1)
	{
		if((*it1)->transformation().updatedInFrame() == frame)
		{
			for(auto it2 = it1 + 1; it2 != b; ++it2)
			{
				checkCollision(*it1, *it2);
			}
		}
	}
}



#endif