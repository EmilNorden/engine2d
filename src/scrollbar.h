#ifndef SCROLLBAR_H_
#define SCROLLBAR_H_

#include "vector2.h"
#include "aabb.h"
#include "sdl/SDL_events.h"
#include "lua_interop_object.h"
#include <string>

class EventRouter;
class GLRenderer;

class Scrollbar
{
private:
	std::string name_;
	Vector2d size_;
	Vector2d position_;
	int min_, max_, value_;
	bool visible_;

	int scrollAreaHeight_;
	float scrollHeight_;

	AABB minButtonBounds_, maxButtonBounds_;

	void calculateScrollHeight();
	void mouseClick(const LuaInteropObjectPtr &e);
	void calculateButtonBounds();	
public:
	Scrollbar();
	~Scrollbar();

	void init(const std::string &name, EventRouter &events);

	void render(const GLRenderer &renderer);

	int min() const { return min_; }
	void setMin(int min);

	int max() const { return max_; }
	void setMax(int max);

	int value() const { return value_; }

	Vector2d position() const { return position_; }
	void setPosition(const Vector2d &pos);

	Vector2d size() const { return size_; }
	void setSize(const Vector2d &size);
};

#endif
