systemInstances = {foo = 5}

function main()
	renderSetClearColor(1, 1, 0, 0);
	createEntity("emil", "textures/bild.JPG")
	setScale("emil", 0.05, 0.015);
	addEventRouting("mouseclick", "mouseClickHandler", "emil");
	addEventRouting("mousemove", "mouseMoveHandler", "emil");

	x, y = getDimensions("emil");
	
	x2, y2 = renderGetViewportDimensions();

	setEntityPosY("emil", y2 - (y * 2));
end

function mouseClickHandler()
	print("You clicked the mouse!");
end

function mouseMoveHandler()
	--mousex, mousey = getMousePos();
	mousex = foo["x"];
	mousey = foo["y"];
	setEntityPosX("emil", mousex);
	
	print("hello from the NEW lua script! The mouse is currently at ["..mousex..","..mousey.."]");
end
