CXX = g++

GL_LIB  = -L/usr/lib/x86_64-linux-gnu/ -lGL
SDL_LIB = -L/usr/local/lib -lSDL2 -Wl,-rpath=/usr/local/lib
SDL_INCLUDE = -I/usr/local/include/SDL2 -I/usr/include -I/usr/include/lua5.1

CPP_FILES = $(wildcard src/*.cpp)
OBJ_FILES = $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))

CXXFLAGS = -g -c -std=c++11 $(SDL_INCLUDE)
LDFLAGS = $(SDL_LIB) $(GL_LIB) -llua5.1 -lSDL2_ttf -lSDL2_image
EXE = obj/eng

all: $(EXE)

$(EXE): $(OBJ_FILES)
	$(CXX) -o $@ $^ $(LDFLAGS)
	cp -r assets obj/assets

obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) -o $@ $<

clean:
	rm obj/*.o && rm $(EXE)
